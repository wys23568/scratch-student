import React, { PureComponent } from 'react';
import './notification.less';
import { Radio, Row, Modal, Pagination, Button } from 'antd';
import { getNotificationList, hasRead, readyToDelete } from 'Redux/actions/notificationactions.js';
import { connect } from 'react-redux';
import Notificationitem from 'Components/notification/notificationItem.js';
import styled from 'styled-components';
export const Userchoice = styled.div`
   height:100%;
   font-size:16px;
   margin-right:28px;
   display:flex;
   justify-content:flex-start;
   align-items:center;
   color:${props => props.choose ? '#54C3D4' : '#999999'};
   border-bottom:${props => props.choose ? '3px solid #54C3D4' : 'none'};
   cursor: pointer;
   transition: all 0.3s;
   &:hover{
       color:#54C3D4;
   }
`
@connect(({ notification: { notificationList } }) => ({ notificationList }))
class Notification extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            type: 0,
            pageNumber: 0,
            pageSize: 10,
            visible: false,
            notifyIds: null
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const { type, pageNumber, pageSize } = this.state;
        const postdata = {
            type: type ? type : '',
            pageNumber,
            pageSize
        }
        dispatch(getNotificationList(postdata))
    }
    onClickTitle = (type, letter) => {
        const { dispatch } = this.props;
        const { pageSize } = this.state;
        const postdata = {
            type: type ? type : '',
            pageNumber: 0,
            pageSize
        }
        dispatch(getNotificationList(postdata))
        this.setState({ type, letter, pageNumber: 0 })
    }
    onClickRead = () => {
        const { dispatch } = this.props;
        const { pageNumber, pageSize, type } = this.state;
        const postdata = {
            type: type ? type : '',
            pageNumber,
            pageSize,
            notifyIds: '',
            state: 2
        }
        dispatch(hasRead(postdata))
        this.setState({ read: true })
    }
    onChangeComment = (e) => {
        const { dispatch } = this.props;
        const { pageSize } = this.state;
        const postdata = {
            type: e.target.value ? e.target.value : '',
            pageNumber: 0,
            pageSize,
            pageTotal: 0
        }
        dispatch(getNotificationList(postdata))
        this.setState({ type: e.target.value, pageNumber: 0 })
    }
    toWorksDetail = (id, value) => {
        const { dispatch, history } = this.props;
        const { pageNumber, pageSize, type } = this.state;
        const postdata = {
            type: type ? type : '',
            pageNumber,
            pageSize,
            notifyIds: value,
            state: 2
        }
        dispatch(hasRead(postdata))
        history.push(`/works-show/${id}`)
    }
    deleteNotification = (value, pageTotal) => {
        this.setState({ notifyIds: value, visible: true, pageTotal })
    }
    handleOk = () => {
        const { dispatch } = this.props;
        const { pageNumber, pageSize, type, notifyIds, pageTotal } = this.state;
        const parseIntPageNumber = pageTotal > 10 && ((pageTotal - 1) / pageSize === pageNumber) ? (pageNumber - 1) : pageNumber;
        const postdata = {
            type: type ? type : '',
            pageNumber: parseIntPageNumber,
            pageSize,
            notifyIds,
        }
        dispatch(readyToDelete(postdata))
        this.setState({ visible: false, pageNumber: parseIntPageNumber })
    }
    onChangePage = (page) => {
        const { dispatch } = this.props;
        const { pageSize, type } = this.state;
        const postdata = {
            type: type ? type : '',
            pageNumber: page - 1,
            pageSize
        }
        dispatch(getNotificationList(postdata))
        this.setState({ pageNumber: page - 1 })
    }
    handleCancel = () => {
        this.setState({ visible: false })
    }
    render() {
        const { content, extendParam = [], pageable = {} } = this.props.notificationList;
        const { type, letter, pageNumber, pageSize, visible, notifyIds } = this.state;
        const { totalSize = 10 } = pageable;
        const exchangeType = new Map([['A', [0]], ['B', [3, 4]], ['C', [2]], ['D', [1]]]);
        const extendParamFilter = extendParam.filter(item => item.t !== 'E');
        const extendParamIsRead = extendParam.filter(item => item.t === 'E')[0];
        const isAllRead = extendParamIsRead && extendParamIsRead.c;
        return (
            <div className='notification'>
                <div className='notification-nav'><div className='notification-left'>
                    {
                        extendParamFilter.map(item => {
                            let judge = exchangeType.get(item.t).includes(type)
                            return <Userchoice key={item.t} choose={judge}
                                onClick={this.onClickTitle.bind(this, exchangeType.get(item.t)[0], item.t)}
                            >
                                {
                                    `${item.n}\xa0\xa0(${item.c})`
                                }
                            </Userchoice>
                        })
                    }
                </div>
                    <div className='notification-potion'>
                        <div className='notification-potion-parent' style={{ color: !isAllRead ? '#2DBB55' : '#999999' }} onClick={this.onClickRead}>
                            <img
                                className='notification-potion-img'
                                src={`${process.env.PUBLIC_URL}/assets/${!isAllRead ? 'icon-finished' : 'icon-finish'}.png`}
                                alt='' />
                            全部已读
                    </div>
                        <div className='notification-potion-parent' onClick={this.deleteNotification.bind(this, '')}>
                            <img
                                className='notification-potion-img'
                                src={`${process.env.PUBLIC_URL}/assets/icon-delete.png`}
                                alt='' />
                            清除通知
                     </div>
                    </div>
                </div>
                <div className='notification-content'>
                    {letter === 'B' && <Radio.Group
                        onChange={this.onChangeComment}
                        defaultValue={3}
                        style={{ fontSize: '16px', margin: '28px 0 0 28px' }}
                        size={'large'} >
                        <Radio.Button value={3}>我收到的评论</Radio.Button>
                        <Radio.Button value={4}>我发出的评论</Radio.Button>
                    </Radio.Group>}
                    {content && (content.length !== 0 ? <div>{
                        content.map(item => {
                            return <div key={item.id}>
                                <Notificationitem
                                    total={totalSize}
                                    deleteNotification={this.deleteNotification.bind(this)}
                                    data={item}
                                    toWorksDetail={this.toWorksDetail.bind(this)} /></div>
                        })
                    }
                        <div className='notification-content-page'>
                            <Pagination
                                current={pageNumber + 1}
                                pageSize={pageSize}
                                onChange={this.onChangePage}
                                hideOnSinglePage={true}
                                defaultCurrent={1}
                                total={totalSize} />
                        </div></div> :
                        <Row type='flex' justify='center' align='middle' style={{ minHeight: '80vh' }}>
                            <div className='course-blank'>
                                <img style={{ width: '212px', height: '170px' }} src={`${process.env.PUBLIC_URL}/assets/nocourse.png`} alt='' />
                                <div className='course-blank-div'>暂无消息哦~</div>
                            </div>
                        </Row>)}
                </div>
                <Modal
                    width={'386px'}
                    bodyStyle={{ width: '100%', padding: '24px 50px 30px 50px' }}
                    visible={visible}
                    onCancel={this.handleCancel}
                    footer={null}
                    destroyOnClose
                    closable={false}
                >
                    <div className='delete-modal-title'>提醒</div>
                    <div className='delete-modal-content'>{`确定要删除${notifyIds ? '该条消息通知' : '全部消息通知'}吗？`}</div>
                    <div className='delete-modal-parent'>
                        <Button className='delete-modal-parent-button' onClick={this.handleCancel}>取消</Button>
                        <Button className='delete-modal-parent-button' onClick={this.handleOk} type='primary'>删除</Button>
                    </div>
                </Modal>
            </div>
        )
    }
}
export default Notification;