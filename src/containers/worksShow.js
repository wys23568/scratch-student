import React, { PureComponent } from 'react';
import './worksShow.less';
import Worksshowitem from 'Components/Works/worksShowItem.js'
import { connect } from 'react-redux';
import { Row, Col,Card } from 'antd';
import { getWorkList } from 'Redux/actions/worksactions.js';
@connect(({ works: { selectedWorks, newWorks } }) => ({ selectedWorks, newWorks }))
class Worksshow extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getWorkList({ type: '1', pageNumber: '0', pageSize: '12' }))
        dispatch(getWorkList({ type: '3', pageNumber: '0', pageSize: '12' }))
    }
    render() {
        const { selectedWorks, newWorks } = this.props;
        const { content: selectedWorksList = [] } = selectedWorks;
        const { content: newWorksList = [] } = newWorks;
        return (
            <div className='works-show'>
                <Card style={{borderRadius:'10px'}}>
                <Row type='flex' style={{ flexDirection: 'column' }} >
                    <Row className='works-show-row'>
                        <img
                            style={{ width: '29px', height: '25px', marginRight: '12px' }}
                            src={`${process.env.PUBLIC_URL}/assets/icon-imperial-crown.png`} alt='' />精选作品</Row>
                    <Row>
                        {selectedWorksList.map(item => {
                            return <Col key={item.id}
                                style={{
                                    minWidth: '286px',
                                    marginBottom: '26px',
                                    display: 'flex',
                                    justifyContent: 'center'
                                }} span={6}>
                                <Worksshowitem detail={item} />
                            </Col>
                        })
                        }</Row>
                </Row>
                <Row type='flex' style={{ flexDirection: 'column' }} >
                    <Row className='works-show-row'>
                        <img
                            style={{ width: '27px', height: '27px', marginRight: '12px' }}
                            src={`${process.env.PUBLIC_URL}/assets/icon-new.png`} alt='' />最新作品</Row>
                    <Row>
                        {newWorksList.map(item => {
                            return <Col key={item.id}
                                style={{
                                    minWidth: '286px',
                                    marginBottom: '26px',
                                    display: 'flex',
                                    justifyContent: 'center'
                                }} span={6}>
                                <Worksshowitem detail={item} />
                            </Col>
                        })
                        }</Row>
                </Row>
                </Card>
            </div>
        )
    }
}
export default Worksshow;