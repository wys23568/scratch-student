import React, { PureComponent, useState, useEffect, useRef } from 'react';
import './publishWorks.less';
import { Card, Row, Col, Button, Input, Icon, Checkbox, Upload } from 'antd';
import * as message from 'Message';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { getProductionTag, getProductionCover } from 'Redux/actions/worksactions.js';
import { getPublishDetail } from 'Redux/actions/publishactions.js';
import { changeIntro, changeOpenFlag, changeOperateExplain, changeTagIds } from 'Redux/actions/publishactions.js';
import requestApi from 'Config/requestApi.js';
import { postPublishInfo } from 'Service/worksShow.js';
import jumpApi from 'Config/jumpApi.js';
const { TextArea } = Input;
const Usertag = styled.div`
   position: relative;
   height:28px;
   padding:4px 14px;
   font-size:13px;
   color:#54C3D4 ;
   background:#EEF8FB ;
   border-radius:28px;
   margin:0 9px 18px 9px;
   display:flex;
   justify-content:start;
   align-items:center;
   border:1px solid #54C3D4;
   cursor: pointer;
`
const Userimg = styled.img`
width: 150px;
height: 120px;
margin: 5px 5px;
cursor: pointer;
border-radius: 5px;
transition:all 0.3s;
transform:${props => props.hasChoose && 'scale(1.05)'};
box-shadow:${props => props.hasChoose && '2px 3px 16px 1px rgba(4, 0, 0, 0.4)'};
&:hover{
    box-shadow: 2px 3px 16px 1px rgba(4, 0, 0, 0.4);
}
`
@connect(({ works: { productionCover, productionTag }, login: { token }, publish: { publishDetail } }) => ({ productionCover, productionTag, token, publishDetail }))
class Publishworks extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            tags: {},
            coverUrl: '',
            loading: false,
            title: '',
            introduce: '',
            explain: '',
            openFlag: '1',
            imgId: ''
        }
    }
    componentDidMount() {
        const { dispatch, location } = this.props;
        let params = new URLSearchParams(location.search);
        dispatch(getProductionTag())
        dispatch(getProductionCover())
        dispatch(getPublishDetail({ productionId: params.get('productionId') }))
        this.setState({ title: params.get('name'), coverUrl: params.get('coverUrl') })
    }
    onChangeTitle = (e) => {
        this.setState({ title: e.target.value })
    }
    onChangeIntroduce = (e) => {
        const { dispatch } = this.props;
        dispatch(changeIntro(e.target.value))
    }
    onChangeExplain = (e) => {
        const { dispatch } = this.props;
        dispatch(changeOperateExplain(e.target.value))
    }
    onchangeCheck = (e) => {
        const { dispatch } = this.props;
        if (e.target.checked) {
            dispatch(changeOpenFlag(1))
            return
        }
        dispatch(changeOpenFlag(2))
    }
    onClickUsertag = (tags) => {
        const { dispatch } = this.props;
        dispatch(changeTagIds(tags))
    }
    onChangeUpload = (info) => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            this.setState({ coverUrl: info.file.response.data.url, loading: false })
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name}上传失败`);
        }
    }
    beforeUpload = (file) => {
        return new Promise((res, rej) => {
            if (file.size > 1024 * 1024 * 10) {
                message.info('上传图片大小不能超过10M')
                rej(file)
                return
            }
            else if (file.size < 1024 * 3) {
                message.info('上传图片过小')
                rej(file)
                return
            }
            res(file)
        })
    }
    onClickUserimg = (imgUrl, imgId) => {
        this.setState({ coverUrl: imgUrl, imgId })
    }
    onClickPublish = () => {
        const { token, location, history, publishDetail } = this.props;
        const { coverUrl, title } = this.state;
        const { intro = '', operateExplain = '', tagIds = '', openFlag } = publishDetail;
        if (!title) {
            message.info('作品名称为必填~')
            return
        }
        else if (!intro) {
            message.info('作品介绍为必填~')
            return
        }
        else if (!operateExplain) {
            message.info('操作说明为必填~')
            return
        }
        let params = new URLSearchParams(location.search);
        let data = {
            loginToken: token,
            productionId: params.get('productionId'),
            sourceType: '3',
            name: title,
            content: params.get('content'),
            coverUrl,
            intro,
            operateExplain,
            tagIds,
            openFlag: openFlag ? openFlag : 1,
            category: '1'
        }
        postPublishInfo(data).then(function (response) {
            if (response.data.code !== 200) {
                message.error(response.data.error, 1)
                return
            }
            message.success('发布成功', 1)
            history.push(`/works-show/${response.data.data.id}`)
        })
            .catch(function (error) {
                console.log(error);
            });


    }
    onClickBack = () => {
        const { location } = this.props;
        let params = new URLSearchParams(location.search);
        window.location.href = `${jumpApi}?type=3&productionId=${params.get('productionId')}`
    }
    render() {
        const { productionCover, productionTag, token, publishDetail } = this.props;
        const { intro = '', operateExplain = '', tagIds = '', openFlag } = publishDetail
        const { coverUrl, loading, title, imgId } = this.state;
        return (
            <div className='publish-works'>
                <Card style={{ borderRadius: '10px' }}>
                    <div className='publish-parent'>
                        <div className='publish-change'>
                            <div className='publish-title'>发布作品</div>
                            <div className='publish-works-parent'>
                                <div className='publish-works-left'><span style={{ color: 'red' }}>*</span>作品名称：</div>
                                <Input
                                    value={title}
                                    maxLength={24}
                                    onChange={this.onChangeTitle}
                                    placeholder='请输入作品名称'
                                    style={{ width: '186px' }}>
                                </Input>
                            </div>
                            <div className='publish-works-parent'>
                                <div className='publish-works-left'><span style={{ color: 'red' }}>*</span>作品介绍：</div>
                                <TextArea
                                    value={intro}
                                    onChange={this.onChangeIntroduce}
                                    maxLength={150}
                                    placeholder='介绍一下你的作品吧，让大家知道你的初衷是什么，分享你的想法～'
                                    rows={6}
                                    className='publish-works-right'>
                                </TextArea>
                            </div>
                            <div className='publish-works-parent'>
                                <div className='publish-works-left'><span style={{ color: 'red' }}>*</span>操作说明：</div>
                                <TextArea
                                    value={operateExplain}
                                    onChange={this.onChangeExplain}
                                    maxLength={150}
                                    placeholder='如果你的作品需要进一步的操作，请不要忘记告诉小伙伴们你的技巧哦～'
                                    rows={6}
                                    className='publish-works-right'>
                                </TextArea>
                            </div>
                            <div className='publish-works-parent'>
                                <div className='publish-works-left'>作品标签：</div>
                                <div className='publish-works-right'>
                                    <Tagsshow productionTag={productionTag} tagIds={tagIds} dispatchTags={this.onClickUsertag.bind(this)} />
                                </div>
                            </div>
                            <div className='publish-code'>
                                <Checkbox
                                    checked={Number(openFlag) !== 2}
                                    onChange={this.onchangeCheck}>
                                    开放源代码
                                </Checkbox>
                            </div>
                        </div>
                        <div className='publish-stable'>
                            <img src={coverUrl} style={{ width: '480px', height: '300px', borderRadius: '5px' }} alt='' />
                            <Upload
                                accept='image/*'
                                action={`${requestApi}/haimawang/account/student/uploadFile`}
                                data={(file) => ({ loginToken: token, upload: file })}
                                showUploadList={false}
                                onChange={this.onChangeUpload}
                                beforeUpload={this.beforeUpload}
                            >
                                <div className='publish-stable-upload'>
                                    <Icon type={loading ? 'loading' : 'plus'} />
                                    <div style={{ paddingLeft: '6px' }}>上传照片</div>
                                </div>
                            </Upload>
                            <div className='publish-cover'>
                                {
                                    productionCover.map(item => {
                                        return <Userimg
                                            hasChoose={imgId === item.id}
                                            onClick={this.onClickUserimg.bind(this, item.imgUrl, item.id)}
                                            key={item.id} src={item.imgUrl}
                                            alt='' />
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <Row type='flex' justify='space-between' align='middle'>
                        <Col onClick={this.onClickBack} className='row-col1' span={8}>
                            <img
                                style={{ width: '22px', height: '18px', marginRight: '6px' }}
                                src={`${process.env.PUBLIC_URL}/assets/icon-goback.png`} alt='' />
                            返回创作页</Col>
                        <Button
                            onClick={this.onClickPublish}
                            className='row-col2'
                            type='primary'>发布</Button>
                        <Col span={8}></Col>
                    </Row>
                </Card>
            </div>
        )
    }
}
export default Publishworks;
const Tagsshow = React.memo(
    ({ productionTag = [], tagIds, dispatchTags }) => {
        const [tags, setTags] = useState({})
        const preTags = usePrevious(tags)
        useEffect(() => {
            let defaultTags = {}
            tagIds && tagIds.split(',').forEach(item => defaultTags[item] = true)
            tagIds && setTags(defaultTags)

        }, [tagIds])
        useEffect(() => {
            if (JSON.stringify(tags) !== JSON.stringify(preTags)) {
                let newIds = []
                Object.keys(tags).forEach(item => {
                    tags[item] && newIds.push(item)
                })
                dispatchTags(newIds.join(','))
            }
        })
        const onClickUsertag = (id) => {
            setTags(preTags => ({
                ...preTags,
                [id]: !tags[id]
            }))
        }
        return (
            <div className='tags-show'>
                {
                    productionTag.map(item => {
                        return <Usertag key={item.id} onClick={() => onClickUsertag(item.id)}>
                            <img style={{ width: '18px', height: '14px', marginRight: '3px' }} src={item.imgUrl} alt='' />
                            {item.name}
                            {tags[item.id] && <Icon className='tags-show-icon' type="check-circle" theme="filled" />}
                        </Usertag>
                    })
                }
            </div>
        )
    }
)
function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}