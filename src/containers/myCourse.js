// import React, { PureComponent } from 'react';
// import { Row, Card, Col, Button, Icon, Modal, Rate } from 'antd';
// import * as message from 'Message';
// import styled from 'styled-components';
// import jumpApi from 'Config/jumpApi.js';
// import './myCourse.less';
// import { closeShowGradeBox, postGradeForTeacher, openShowGradeBox, getCourseDetail } from 'Redux/actions/courseactions.js';
// import { connect } from 'react-redux';
// import moment from 'moment';
// import Myvideo from 'Components/Myvideo/Myvideo.js'
// export const Scratch = styled.div`
// color:#555555;
// display:flex;
// font-size:16px;
// align-items:center;
// `
// const Userlistitem = styled.div`
// border-radius: 5px;
// width:286px;
// position:relative;
// transition:all 0.3s;
// transform:${props => props.hasChoose && 'scale(1.05)'};
// box-shadow:${props => props.hasChoose && '2px 3px 16px 1px rgba(4, 0, 0, 0.4)'};
// &:hover{
//     box-shadow: 2px 3px 16px 1px rgba(4, 0, 0, 0.4);
// }
// `
// @connect(({ course: { showGradeBox, courseDetail } }) => ({ showGradeBox, courseDetail }))
// class Mycourse extends PureComponent {
//     constructor(props) {
//         super(props)
//         this.state = {
//             data: {
//                 courseId: '',
//                 id: '',
//                 name: '',
//                 knowledges: '',
//                 studentCourseHomeworkInfo: { studyFlag: '' },
//                 coverUrl: '',
//                 videoUrl: '',
//                 lockFlag: 2,
//                 teacherCourseId: ''
//             },
//             visible: false,
//             rateValue: 5
//         }
//     }
//     componentDidMount() {
//         const { dispatch, location } = this.props;
//         let params = new URLSearchParams(location.search);
//         dispatch(getCourseDetail({ courseId: params.get('courseId'), teacherCourseId: params.get('teacherCourseId') }))
//     }
//     static getDerivedStateFromProps(nextProps, prevState) {
//         if (!prevState.data.courseId && nextProps.courseDetail) {
//             return {
//                 ...prevState,
//                 data: nextProps.courseDetail.courseLessonInfoList[0]
//             }
//         }
//         return null
//     }
//     courseClick = (data, studentScore, stopFlag) => {
//         const { dispatch } = this.props;
//         !studentScore && stopFlag === 2 && dispatch(openShowGradeBox())
//         this.setState({
//             data
//         })
//     }
//     showModal = () => {
//         document.body.style.overflow = 'hidden'
//         this.setState({
//             visible: true,
//         });
//     }
//     handleCancel = (e) => {
//         document.body.style.overflow = 'auto'
//         this.setState({
//             visible: false,
//         });
//     }
//     toStudyOrCreate = ({ state }) => {
//         const { history } = this.props;
//         const { courseId, id, studentCourseHomeworkInfo: { teacherCourseId } } = this.state.data;
//         if (state > 1) {
//             history.push({
//                 pathname: `/personal-center/my-homework/${id}`,
//                 search: `?courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`
//             })
//         }
//         else {
//             window.location.href = (`${jumpApi}?type=2&courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`)
//         }
//     }
//     closeGradeBox = () => {
//         const { dispatch } = this.props;
//         dispatch(closeShowGradeBox())
//     }
//     onChangeRateValue = (value) => {
//         this.setState({ rateValue: value })
//     }
//     onClickUploadGrade = (courseId, teacherCourseId) => {
//         const { rateValue } = this.state;
//         const { dispatch } = this.props;
//         if (!rateValue) {
//             message.info('我们的老师很辛苦，给个合适的评分呗~', 1)
//             return
//         }
//         dispatch(postGradeForTeacher({ courseId, teacherCourseId, score: rateValue * 20 }))
//     }
//     render() {
//         const { showGradeBox, courseDetail } = this.props;
//         const { data: { teacherCourseId, knowledges, name, id, studentCourseHomeworkInfo, coverUrl, videoUrl, lockFlag }, visible, rateValue } = this.state;
//         return (
//             <div className='course'>
//                 {courseDetail && <div>
//                     <Card>
//                         <Scratch>{courseDetail.name}</Scratch>
//                         <div className='parent'>
//                             <div className='stable'>
//                                 <img style={{ width: '100%', height: '100%', borderRadius: '5px' }} src={coverUrl} alt='' />
//                                 <div className='stable-icon'>
//                                     {lockFlag === 1 ? <Icon onClick={() => { message.info('课程未开放！') }} type="lock" theme="filled" /> :
//                                         <Icon onClick={this.showModal} type="play-circle" />}
//                                 </div>
//                             </div>
//                             <div className='change'>
//                                 <Row>
//                                     <div className='change-row1'>
//                                         <div style={{ marginRight: '28px' }}>
//                                             {`第${id}课时`}
//                                         </div>
//                                         {name}
//                                     </div>
//                                     <div className='change-row2'>课程知识点：</div>
//                                     <div className='change-row3'>{knowledges}</div>
//                                 </Row>
//                                 <Row type='flex' justify='end' align='middle'>
//                                     <Button
//                                         disabled={lockFlag === 1}
//                                         onClick={this.toStudyOrCreate.bind(this, studentCourseHomeworkInfo)}
//                                         type='primary'
//                                         style={{ minWidth: '136px', height: '38px', borderRadius: '38px' }}>
//                                         {studentCourseHomeworkInfo && studentCourseHomeworkInfo.state > 1 ? '查看作业作品' : '开始学习'}</Button></Row>
//                             </div>
//                         </div>
//                     </Card>
//                     <Row className='course-row' type='flex' justify='start'>
//                         {
//                             courseDetail.courseLessonInfoList.map(item => {
//                                 return (
//                                     <Col className='course-row-col'
//                                         onClick={this.courseClick.bind(this, item, courseDetail.studentScore, courseDetail.stopFlag)}
//                                         span={6}
//                                         key={item.id}>
//                                         <Userlistitem hasChoose={item.id === id}><img src={item.coverUrl}
//                                             style={{ width: '100%', height: '196px', borderRadius: '5px' }} alt='' />
//                                             <div className='course-row-col-header'>{`第${item.id}课时`}</div>
//                                             {item.lockFlag === 1 && <div className='course-row-col-icon'>
//                                                 <Icon style={{ fontSize: '66px', marginBottom: '8px' }} type="lock" theme="filled" />
//                                                 <div style={{ textAlign: 'center' }}>
//                                                     {`待开放\xa0${moment(item.startAt).format('YYYY年MM月DD日\xa0HH:mm')}`}
//                                                 </div>
//                                             </div>}
//                                         </Userlistitem>
//                                     </Col>
//                                 )
//                             })
//                         }
//                     </Row>
//                 </div>}
//                 {visible && <div onClick={this.handleCancel} className='show-background'>
//                     <div onClick={(e) => { e.stopPropagation() }} className='show-video-content'>
//                     <Myvideo videoUrl={videoUrl} handleCancel={this.handleCancel.bind(this)} />
//                     </div>
//                 </div>
//                 }
//                 <Modal
//                     width={280}
//                     visible={showGradeBox}
//                     footer={null}
//                     onCancel={this.closeGradeBox}
//                     bodyStyle={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'column' }}
//                 >
//                     <div className='grade-modal-title'>
//                         <img
//                             style={{ width: '20px', height: '20px' }}
//                             src={`${process.env.PUBLIC_URL}/assets/icon-smile-grade.png`} alt='' />{`\xa0恭喜你学完全部课程！`}</div>
//                     <div style={{ fontSize: '16px' }}>给辅导老师打个分吧！</div>
//                     <div className='grade-modal-content'>我的评分：
//                     <Rate allowClear={false} allowHalf onChange={this.onChangeRateValue} value={rateValue} /></div>
//                     <Button className='grade-modal-button' type='primary' onClick={this.onClickUploadGrade.bind(this, id, teacherCourseId)}>上传评分</Button>
//                 </Modal>
//             </div>
//         )
//     }
// }
// export default Mycourse;