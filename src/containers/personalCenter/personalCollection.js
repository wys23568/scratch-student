import React, { PureComponent } from 'react';
import './personalCollection.less';
import { connect } from 'react-redux';
import PersonalCenterNav from 'Components/personalCenter/personalCenterNav.js';
import { getMyCollectionList, getMyLikeList } from 'Redux/actions/personalactions.js';
import { Row, Col, Pagination } from 'antd';
import Blank from 'Components/blank/blank.js';
import Singleshow from 'Components/personalCenter/singleShow.js';
import { Userchoice } from '../notification.js';
import routerApi from 'Config/routerApi.js';
@connect(({ login: { userInfo }, personal: { myCollectionList, myLikeList } }) => ({ userInfo, myCollectionList, myLikeList }))
class Personalcollection extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 0,
            pageSize: 12,
            collectionOrLike: 'A'
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const { pageNumber, pageSize } = this.state;
        dispatch(getMyCollectionList({ pageNumber, pageSize }))
    }
    onClickTitle = (collectionOrLike) => {
        const { dispatch } = this.props;
        const { pageSize } = this.state;
        const data = {
            pageNumber: 0,
            pageSize
        }
        collectionOrLike === 'A' ? dispatch(getMyCollectionList(data)) : dispatch(getMyLikeList(data))
        this.setState({ collectionOrLike, pageNumber: 0 })
    }
    toChangePage = ({ id }) => {
        // const { history } = this.props;
        window.open(`${routerApi}#/works-show/${id}`)
        // history.push(`/works-show/${id}`)
    }
    onChangePage = (page) => {
        const { dispatch } = this.props;
        const { collectionOrLike, pageSize } = this.state;
        const data = {
            pageNumber: page - 1,
            pageSize
        }
        collectionOrLike === 'A' ? dispatch(getMyCollectionList(data)) : dispatch(getMyLikeList(data))
        this.setState({ pageNumber: page - 1 })
    }
    render() {
        const { myCollectionList, myLikeList, userInfo } = this.props;
        const { extendParam = [] } = myCollectionList;
        const { collectionOrLike, pageNumber, pageSize } = this.state;
        const renderList = collectionOrLike === 'A' ? myCollectionList : myLikeList;
        const { content, pageable = {} } = renderList;
        const { totalSize = 10 } = pageable;
        return (
            <div className='personal-collection'>
                <div className='personal-collection-left'>
                    <PersonalCenterNav userInfo={userInfo} />
                </div>
                <div className='personal-collection-right'>
                    <div className='personal-collection-right-title'>
                        {
                            extendParam.map(item => {
                                return <Userchoice
                                    key={item.t}
                                    choose={item.t === collectionOrLike}
                                    onClick={this.onClickTitle.bind(this, item.t)}
                                >
                                    {`${item.n}\xa0\xa0(${item.c})`}
                                </Userchoice>
                            })
                        }
                    </div>
                    <Row
                        type='flex'
                        justify='start'
                        className='personal-collection-right-content' >
                        {
                            content && (content.length !== 0 ? content.map(item => {
                                return <Col
                                    className='personal-collection-right-content-item'
                                    key={item.id}
                                    span={6}>
                                    <Singleshow
                                        renderType={3}
                                        data={item}
                                        toChangePage={this.toChangePage} />
                                </Col>
                            }) : <Blank message={'这里还没有作品哦~'} />)
                        }
                    </Row>
                    <div className='notification-content-page'>
                        <Pagination
                            defaultPageSize={12}
                            current={pageNumber + 1}
                            pageSize={pageSize}
                            onChange={this.onChangePage}
                            hideOnSinglePage={true}
                            defaultCurrent={1}
                            total={totalSize} />
                    </div>
                </div>
            </div>
        )
    }
}
export default Personalcollection;