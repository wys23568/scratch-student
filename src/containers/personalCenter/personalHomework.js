import React, { PureComponent } from 'react';
import './personalHomework.less';
import { connect } from 'react-redux';
import PersonalCenterNav from 'Components/personalCenter/personalCenterNav.js';
import { Scratch } from '../myCourseList.js';
import { Row, Col, Pagination } from 'antd';
import * as message from 'Message';
import Blank from 'Components/blank/blank.js';
import jumpApi from 'Config/jumpApi.js';
import Singleshow from 'Components/personalCenter/singleShow.js';
import {
    //  getMyHomeworksList,
    getCourseHomeworkList
} from 'Redux/actions/personalactions.js';

@connect(({ login: { userInfo }, personal: { myHomeworksList } }) => ({ userInfo, myHomeworksList }))
class Personalhomework extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 0,
            pageSize: 12,
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const { pageNumber, pageSize } = this.state;
        // dispatch(getMyHomeworksList({ pageNumber, pageSize, courseId: 9 }))
        dispatch(getCourseHomeworkList({ pageNumber, pageSize}))
    }
    toChangePage = ({ studentCourseHomeworkInfo: { state, teacherCourseId }, courseId, id, lockFlag }) => {
        const { history } = this.props;
        const operationMap = new Map([[1,
            () => {
                lockFlag === 1 ?
                    message.info('您的课程还未到开放时间呢~', 1)
                    :
                    window.location.href = `${jumpApi}?type=2&courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`
            }],
        [2, () => {
            window.location.href = `${jumpApi}?type=2&courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`
        }],
        [3, () => {
            history.push({
                pathname: `/personal-center/my-homework/${courseId}/${id}`,
                search: `?courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`
            })
        }]])
        let actions = operationMap.get(state)
        actions.call(this)
    }
    onChangePage = (page) => {
        const { dispatch } = this.props;
        const { pageSize } = this.state;
        const data = {
            pageNumber: page - 1,
            pageSize
        }
        // dispatch(getMyHomeworksList(data))
        dispatch(getCourseHomeworkList(data))
        this.setState({ pageNumber: page - 1 })
    }
    render() {
        const { myHomeworksList, userInfo } = this.props;
        const { content, pageable = {}, extendParam } = myHomeworksList;
        const { pageNumber, pageSize } = this.state;
        const { totalSize = 10 } = pageable;
        return (
            <div className='personal-homework'>
                <div className='personal-homework-left'>
                    <PersonalCenterNav userInfo={userInfo} />
                </div>
                <div className='personal-homework-right'>
                    {extendParam && <Scratch ><img
                        style={{ width: '22px', height: '22px', marginRight: '9px' }}
                        src={`${process.env.PUBLIC_URL}/assets/icon-scratch-lesson.png`}
                        alt='' />{extendParam.name}</Scratch>}
                    <Row
                        type='flex'
                        justify='start'
                        className='personal-homework-right-content' >
                        {
                            content && (content.length !== 0 ? content.map(item => {
                                return <Col
                                    className='personal-homework-right-content-item'
                                    key={item.id}
                                    span={6}>
                                    <Singleshow
                                        renderType={2}
                                        data={item}
                                        toChangePage={this.toChangePage}
                                    />
                                </Col>
                            }) : <Blank message={'你还没有作业哦~'} />)
                        }
                    </Row>
                    <div className='notification-content-page'>
                        <Pagination
                            defaultPageSize={12}
                            current={pageNumber + 1}
                            pageSize={pageSize}
                            onChange={this.onChangePage}
                            hideOnSinglePage={true}
                            defaultCurrent={1}
                            total={totalSize} />
                    </div>
                </div>
            </div>
        )
    }
}
export default Personalhomework;