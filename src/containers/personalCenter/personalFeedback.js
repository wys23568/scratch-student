import React, { PureComponent } from 'react';
import './personalFeedback.less';
import { connect } from 'react-redux';
import { Input, Icon, Upload, Button, Modal } from 'antd';
import * as message from 'Message';
import PersonalCenterNav from 'Components/personalCenter/personalCenterNav.js';
import requestApi from 'Config/requestApi.js';
import { questionCommit } from 'Service/personal.js';
const { TextArea } = Input;
@connect(({ login: { userInfo, token } }) => ({ userInfo, token }))
class Personalfeedback extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            fileList: [],
            title: '',
            content: '',
            qq: '',
            previewVisible: false,
            previewImage: '',
        }
    }
    onChangeUpload = (info) => {
        this.setState({ fileList: info.fileList })
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            this.setState({ loading: false })
        } else if (info.file.status === 'error') {
            this.setState({ loading: false })
            message.error(`${info.file.name}上传失败`);
        }
    }
    beforeUpload = (file) => {
        return new Promise((res, rej) => {
            if (file.size > 1024 * 1024 * 10) {
                message.info('上传图片大小不能超过10M')
                rej(file)
                return
            }
            else if (file.size < 1024 * 3) {
                message.info('上传图片过小')
                rej(file)
                return
            }
            res(file)
        })
    }
    onClickSubmit = () => {
        const { title, fileList, content, qq } = this.state;
        const { token, history } = this.props;
        const mail = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
        let postFileList = [];
        if (!title) {
            message.info('请填写标题', 1)
            return
        }
        else if (!content) {
            message.info('请填写反馈内容', 1)
            return
        }
        else if ((!/^[1-9][0-9]{4,11}$/.test(qq)) && (!mail.test(qq))) {
            message.error('请填写正确的联系方式', 1)
            return
        }
        fileList.forEach(item => item.status === 'done' && postFileList.push(item.response.data.url))
        const data = {
            loginToken: token,
            title, content, contact: qq, imgUrl: postFileList.join(',')
        }
        questionCommit(data).then(function (response) {
            if (response.data.code !== 200) {
                message.error(response.data.error, 1)
                return
            }
            message.success('感谢您的支持', 1)
            history.push('/personal-center/my-works')
        })
            .catch(function (error) {
                console.log(error);
            });
    }
    onChangeTitle = (e) => {
        this.setState({ title: e.target.value })
    }
    onChangeContent = (e) => {
        this.setState({ content: e.target.value })
    }
    onChangeQQ = (e) => {
        this.setState({ qq: e.target.value })
    }
    handleCancel = () => this.setState({ previewVisible: false });
    handlePreview = file => {
        this.setState({
            previewImage: file.response.data.url || file.thumbUrl,
            previewVisible: true,
        });
    }
    handleDelete = (file) => {
        this.setState({ fileList: this.state.fileList.filter(item => item.uid !== file.uid) })
    }
    render() {
        const { userInfo, token } = this.props;
        const { previewVisible, previewImage, fileList } = this.state;
        const fileListRender=fileList.filter(item=>item.status!=='error')
        return (
            <div className='personal-feedback'>
                <div className='personal-feedback-left'>
                    <PersonalCenterNav userInfo={userInfo} />
                </div>
                <div className='personal-feedback-right'>
                    <div className='right-big-title'>问题反馈</div>
                    <div className='right-row' style={{ alignItems: 'center' }}>
                        <div className='right-row-left'><span style={{ color: 'red' }}>*</span>标题：</div>
                        <div className='change'> <Input
                            onChange={this.onChangeTitle}
                            style={{ width: '286px' }}
                            placeholder='请输入标题'
                        />
                        </div>
                    </div>
                    <div className='right-row'>
                        <div className='right-row-left'><span style={{ color: 'red' }}>*</span>内容：</div>
                        <div className='change'> <TextArea
                            onChange={this.onChangeContent}
                            maxLength={300}
                            rows={8}
                            placeholder='请填写具体内容（如：哪个作品或哪个页面）帮助我们了解你的意见与建议。'
                        />
                            <Upload
                                accept='image/*'
                                action={`${requestApi}/haimawang/account/student/uploadFile`}
                                data={(file) => ({ loginToken: token, upload: file })}
                                onChange={this.onChangeUpload}
                                beforeUpload={this.beforeUpload}
                                showUploadList={false}
                                fileList={fileList}
                            >
                                <div className='personal-feedback-upload'>
                                    <Icon style={{ fontSize: '18px', marginRight: '6px' }} type="picture" />添加截图
                                </div>
                            </Upload>
                            <div className='picture'>{
                                fileListRender.length > 0 && fileListRender.map(item => {
                                    return <div className='picture-item'
                                        key={item.uid}>
                                        {item.status === 'uploading' ? <Icon type="loading" /> :
                                            (item.status === 'done' ? <img
                                                onClick={this.handlePreview.bind(this, item)}
                                                style={{ width: '100%', height: '100%', cursor: 'pointer' }}
                                                src={item.response.data.url}
                                                alt='' /> : null)}
                                        {item.status === 'done' && <Icon
                                            onClick={this.handleDelete.bind(this, item)}
                                            style={{
                                                position: 'absolute',
                                                fontSize: '14px',
                                                top: '-6px',
                                                right: '-6px',
                                                color: '#BFBFBF',
                                                background: '#fff',
                                                borderRadius: '14px'
                                            }}
                                            type="close-circle"
                                            theme="filled" />}
                                    </div>
                                })
                            }</div>
                            <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                <img alt='' style={{ width: '100%' }} src={previewImage} />
                            </Modal>
                        </div>
                    </div>
                    <div className='right-row' style={{ alignItems: 'center' }}>
                        <div className='right-row-left'><span style={{ color: 'red' }}>*</span>联系方式：</div>
                        <div className='change'>
                            <Input
                                style={{ width: '286px' }}
                                placeholder='QQ/邮箱'
                                onChange={this.onChangeQQ}
                            />
                        </div>
                    </div>
                    <div className='right-row-comfirm'>
                        <Button
                            onClick={this.onClickSubmit}
                            className='right-row-comfirm-button'
                            type='primary'>提交</Button>
                    </div>
                </div>
            </div>
        )
    }
}
export default Personalfeedback;