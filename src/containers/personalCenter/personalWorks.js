import React, { PureComponent } from 'react';
import './personalWorks.less';
import { connect } from 'react-redux';
import PersonalCenterNav from 'Components/personalCenter/personalCenterNav.js';
import Singleshow from 'Components/personalCenter/singleShow.js';
import { getMyWorksList, deleteMyWorks } from 'Redux/actions/personalactions.js';
import { Userchoice } from '../notification.js';
import { Row, Col, Modal, Pagination, Button } from 'antd';
import Blank from 'Components/blank/blank.js';
import jumpApi from 'Config/jumpApi.js';
import routerApi from 'Config/routerApi.js';
@connect(({ login: { userInfo }, personal: { myWorksList } }) => ({ userInfo, myWorksList }))
class Personalworks extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            state: 2,
            pageNumber: 0,
            pageSize: 12,
            visible: false,
            name: '',
            pageTotal: 0
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const { state, pageNumber, pageSize } = this.state;
        const data = {
            state,
            pageNumber,
            pageSize
        }
        dispatch(getMyWorksList(data))
    }
    onClickTitle = (state) => {
        const { dispatch } = this.props;
        const { pageSize } = this.state;
        const data = {
            state,
            pageNumber: 0,
            pageSize
        }
        dispatch(getMyWorksList(data))
        this.setState({ state, pageNumber: 0 })
    }
    onEdit = ({ id }) => {
        window.location.href = `${jumpApi}?type=3&productionId=${id}`
    }
    onDelete = ({ id, name }, pageTotal) => {
        this.setState({ visible: true, name, productionId: id, pageTotal })
    }
    handleOk = () => {
        const { dispatch } = this.props;
        const { pageSize, state, pageNumber, productionId, pageTotal } = this.state;
        const parseIntPageNumber = pageTotal > 10 && ((pageTotal - 1) / pageSize === pageNumber) ? (pageNumber - 1) : pageNumber;
        const data = {
            state,
            pageNumber: parseIntPageNumber,
            pageSize,
            productionId
        }
        dispatch(deleteMyWorks(data))
        this.setState({ visible: false, pageNumber: parseIntPageNumber })
    }
    onPublish = ({ id, content, name, coverUrl }) => {
        const { history } = this.props;
        history.push({
            pathname: '/publish-works',
            search: `?productionId=${id}&content=${content}&name=${name}&coverUrl=${coverUrl}`
        })
    }
    toChangePage = ({ id, content, name, coverUrl, state }) => {
        const { history } = this.props;
        if (state === 1) {
            history.push({
                pathname: '/publish-works',
                search: `?productionId=${id}&content=${content}&name=${name}&coverUrl=${coverUrl}`
            })
        }
        else {
            window.open(`${routerApi}#/works-show/${id}`)
            // history.push(`/works-show/${id}`)
        }
    }
    onChangePage = (page) => {
        const { dispatch } = this.props;
        const { state, pageSize } = this.state;
        const data = {
            state,
            pageNumber: page - 1,
            pageSize
        }
        dispatch(getMyWorksList(data))
        this.setState({ pageNumber: page - 1 })
    }
    handleCancel = () => {
        this.setState({ visible: false })
    }
    render() {
        const { myWorksList, userInfo } = this.props;
        const { content, extendParam = [], pageable = {} } = myWorksList;
        const { state, pageNumber, pageSize, visible, name = '' } = this.state;
        const { totalSize = 10 } = pageable;
        const exchangeType = new Map([['A', 1], ['B', 2], ['C', 3]])
        return (
            <div className='personal-works'>
                <div className='personal-works-left'>
                    <PersonalCenterNav userInfo={userInfo} />
                </div>
                <div className='personal-works-right'>
                    <div className='personal-works-right-title'>
                        {
                            extendParam.map(item => {
                                return <Userchoice
                                    key={item.t}
                                    choose={exchangeType.get(item.t) === state}
                                    onClick={this.onClickTitle.bind(this, exchangeType.get(item.t))}
                                >
                                    {`${item.n}\xa0\xa0(${item.c})`}
                                </Userchoice>
                            })
                        }
                    </div>
                    <Row
                        type='flex'
                        justify='start'
                        className='personal-works-right-content' >
                        {
                            content && (content.length !== 0 ? content.map(item => {
                                return <Col
                                    className='personal-works-right-content-item'
                                    key={item.id}
                                    span={6}>
                                    <Singleshow
                                        total={totalSize}
                                        renderType={1}
                                        data={item}
                                        onEdit={this.onEdit}
                                        onDelete={this.onDelete}
                                        onPublish={this.onPublish}
                                        toChangePage={this.toChangePage} />
                                </Col>
                            }) : <Blank message={'这里还没有作品哦~'} />)
                        }
                    </Row>
                    <div className='notification-content-page'>
                        <Pagination
                            defaultPageSize={12}
                            current={pageNumber + 1}
                            pageSize={pageSize}
                            onChange={this.onChangePage}
                            hideOnSinglePage={true}
                            defaultCurrent={1}
                            total={totalSize} />
                    </div>
                </div>
                <Modal
                    width={'386px'}
                    bodyStyle={{ width: '100%', padding: '24px 50px 30px 50px' }}
                    visible={visible}
                    onCancel={this.handleCancel}
                    footer={null}
                    destroyOnClose
                    closable={false}
                >
                    <div className='delete-modal-title'>提醒</div>
                    <div className='delete-modal-content'>{`确定要删除作品“${name}”吗？`}</div>
                    <div className='delete-modal-parent'>
                        <Button className='delete-modal-parent-button' onClick={this.handleCancel}>取消</Button>
                        <Button className='delete-modal-parent-button' onClick={this.handleOk} type='primary'>删除</Button>
                    </div>
                </Modal>
            </div>
        )
    }
}
export default Personalworks;