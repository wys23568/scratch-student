import React, { PureComponent } from 'react';
import './personalOrder.less';
import { connect } from 'react-redux';
import PersonalCenterNav from 'Components/personalCenter/personalCenterNav.js';
import { getMyOrderList, doApplyRefund } from 'Redux/actions/personalactions.js';
import { Row, Col, Pagination } from 'antd';
import Blank from 'Components/blank/blank.js';
import Orderitem from 'Components/personalCenter/orderItem.js';
@connect(({ login: { userInfo }, personal: { myOrderList } }) => ({ userInfo, myOrderList }))
class Personalorder extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 0,
            pageSize: 10
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const { pageNumber, pageSize } = this.state;
        dispatch(getMyOrderList({ pageNumber, pageSize }))
    }
    onApplyRefund = (studentCourseOrderId) => {
        const { dispatch } = this.props;
        const { pageNumber, pageSize } = this.state;
        const data = {
            studentCourseOrderId,
            pageNumber,
            pageSize
        }
        dispatch(doApplyRefund(data))
    }
    onChangePage = (page) => {
        const { dispatch } = this.props;
        const { pageSize } = this.state;
        const data = {
            pageNumber: page - 1,
            pageSize
        }
        dispatch(getMyOrderList(data))
        this.setState({ pageNumber: page - 1 })
    }
    render() {
        const { myOrderList, userInfo } = this.props;
        const { content, pageable = {} } = myOrderList;
        const { totalSize = 10 } = pageable;
        const { pageNumber, pageSize } = this.state;
        return (
            <div className='personal-order'>
                <div className='personal-order-left'>
                    <PersonalCenterNav userInfo={userInfo} />
                </div>
                <div className='personal-order-right'>
                    {
                        content && (content.length !== 0 ? <div style={{ padding: '24px' }}>
                            <div className='personal-order-right-title'>我的订单</div>
                            <Row
                                type='flex'
                                justify='start'
                                align='middle'
                                className='personal-order-right-row'
                            >
                                <Col span={8}>课程名称</Col>
                                <Col span={4}>期次</Col>
                                <Col span={4}>订单总额</Col>
                                <Col span={4}>订单状态</Col>
                                <Col span={4}>其他操作</Col>
                            </Row>
                            {
                                content.map(item => {
                                    return <Orderitem key={item.id} data={item} toApplyRefund={this.onApplyRefund.bind(this)} />
                                })
                            }
                            <div className='notification-content-page'>
                                <Pagination
                                    defaultPageSize={12}
                                    current={pageNumber + 1}
                                    pageSize={pageSize}
                                    onChange={this.onChangePage}
                                    hideOnSinglePage={true}
                                    defaultCurrent={1}
                                    total={totalSize} />
                            </div>
                        </div> : <Blank message={'你还没有订单哦~'} />)
                    }
                </div>
            </div>
        )
    }
}
export default Personalorder;