import React, { PureComponent } from 'react';
import './personalAccount.less';
import { connect } from 'react-redux';
import PersonalCenterNav from 'Components/personalCenter/personalCenterNav.js';
import { Button, Icon, Upload, Input, Radio, Modal } from 'antd';
import * as message from 'Message';
import requestApi from 'Config/requestApi.js';
import {
    updateUserInfo,
    getPersonalInfo,
    updateAvatarUrl,
    updateGender,
    updateName,
    phoneUnbinding,
    phoneUnbindingComfirm,
    phoneBinding,
    phoneBindingComfirm
} from 'Redux/actions/personalactions.js';
import Phonechange from 'Components/personalCenter/phoneChange.js';
const RadioGroup = Radio.Group;
@connect(({ login: { token, userInfo }, personal: { personalInfo, newOrOldType } }) => ({ personalInfo, token, userInfo, newOrOldType }))
class Personalaccount extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            visible: false,
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getPersonalInfo())
    }
    onChangeUpload = (info) => {
        const { dispatch } = this.props;
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            dispatch(updateAvatarUrl(info.file.response.data.url))
            this.setState({ loading: false })
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name}上传失败`);
        }
    }
    beforeUpload = (file) => {
        return new Promise((res, rej) => {
            if (file.size > 1024 * 1024 * 10) {
                message.info('上传图片大小不能超过10M')
                rej(file)
                return
            }
            else if (file.size < 1024 * 3) {
                message.info('上传图片过小')
                rej(file)
                return
            }
            res(file)
        })
    }
    onChangeName = (e) => {
        const { dispatch } = this.props;
        dispatch(updateName(e.target.value))
    }
    onChangeGender = (e) => {
        const { dispatch } = this.props;
        dispatch(updateGender(e.target.value))
    }
    onClickSaveInfo = () => {
        const { dispatch, personalInfo } = this.props;
        const { avatarUrl, name, phone, gender } = personalInfo;
        if (!name) {
            message.error('用户名不能为空', 1)
            return
        }
        dispatch(updateUserInfo({ avatarUrl, name, phone, gender }))

    }
    onChangePhone = (phone) => {
        const { dispatch } = this.props;
        dispatch(updateGender(phone))
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    }
    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }
    getCode = (number, newOrOldType) => {
        const { dispatch } = this.props;
        if (newOrOldType) {
            dispatch(phoneBinding({ verifyPhone: number }))
            return
        }
        dispatch(phoneUnbinding())
    }
    oldComfirm = (number, code) => {
        const { dispatch } = this.props;
        dispatch(phoneUnbindingComfirm({ verifyCode: code }))
    }
    newComfirm = (number, code) => {
        const { dispatch } = this.props;
        dispatch(phoneBindingComfirm({ verifyCode: code, verifyPhone: number }));
        this.setState({ visible: false })
    }
    render() {
        const { token, personalInfo, userInfo, newOrOldType } = this.props;
        const { avatarUrl = '', name = '', gender = '', phone = '' } = personalInfo;
        const { loading, visible } = this.state;
        return (
            <div className='personal-account'>
                <div className='personal-account-left'>
                    <PersonalCenterNav userInfo={userInfo} />
                </div>
                <div className='personal-account-right'>
                    <div className='personal-account-title'>基本信息</div>
                    <div className='personal-account-avatar'>
                        <div className='personal-account-font'><span>头</span><span>像：</span></div>
                        <img className='personal-account-avatar-img' src={avatarUrl} alt='' />
                        <div className='personal-account-avatar-edit'>
                            <Upload
                                accept='image/*'
                                action={`${requestApi}/haimawang/account/student/uploadFile`}
                                data={(file) => ({ loginToken: token, upload: file })}
                                showUploadList={false}
                                onChange={this.onChangeUpload}
                                beforeUpload={this.beforeUpload}
                            >
                                <Icon style={{ color: '#54C3D4', cursor: 'pointer' }} type={loading ? 'loading' : 'edit'} />
                            </Upload>
                        </div>
                    </div>
                    <div className='personal-account-input'>
                        <div className='personal-account-font'>用户名：</div>
                        <Input
                            maxLength={16}
                            onChange={this.onChangeName}
                            value={name}
                            style={{ width: '206px' }}
                            placeholder='请输入您的用户名'
                        />
                    </div>
                    <div className='personal-account-input'>
                        <div className='personal-account-font'>
                            <span>性</span><span>别：</span>
                        </div>
                        <RadioGroup value={gender} onChange={this.onChangeGender}>
                            <Radio value={1}>男</Radio>
                            <Radio value={2}>女</Radio>
                        </RadioGroup>
                    </div>
                    <div className='personal-account-button'>
                        <Button onClick={this.onClickSaveInfo} className='personal-account-button-style' type='primary'>保存</Button>
                    </div>
                    <div className='personal-account-title'>手机换绑</div>
                    <div className='personal-account-phone'>{phone && `已绑定手机号：\xa0${phone.replace(/(\d{3})\d{4}(\d{4})/, '$1****$2')}`}</div>
                    <div className='personal-account-button'>
                        <Button
                            onClick={this.showModal}
                            className='personal-account-button-style'
                            type='primary'>
                            修改
                        </Button>
                    </div>
                </div>
                <Modal
                    width={'400px'}
                    visible={visible}
                    footer={null}
                    closable={false}
                    onCancel={this.handleCancel}
                    destroyOnClose={true}
                >
                    <Phonechange
                        oldPhone={phone}
                        key={newOrOldType}
                        type={newOrOldType}
                        getCode={this.getCode.bind(this)}
                        oldComfirm={this.oldComfirm.bind(this)}
                        newComfirm={this.newComfirm.bind(this)}
                        placeholder={newOrOldType ? '新手机号' : '原手机号'}
                    />
                </Modal>
            </div>
        )
    }
}
export default Personalaccount;