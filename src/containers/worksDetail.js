import React, { PureComponent } from 'react';
import './worksDetail.less';
import {
    withRouter
} from "react-router-dom";
import { Row, Col, Icon, Popover, Card, Button, Modal, Rate, Pagination } from 'antd';
import { getWorkList } from 'Redux/actions/worksactions.js'
import styled from 'styled-components';
import Commentbox from 'Components/Comment/commentBox.js';
import Commentitem from 'Components/Comment/commentItem.js';
import { connect } from 'react-redux';
import {
    getWorkDetail,
    doLike,
    cancelLike,
    doCollected,
    cancelCollected,
    getCommentList,
    getReplyList,
    postComment,
    postReply,
    doCommentLike,
    cancelCommentLike,
    doReplyLike,
    cancelReplyLike,
    deleteComment,
    deleteReply
} from 'Redux/actions/worksactions.js';
import { showLoginModal } from 'Redux/actions/loginactions.js';
import ReportBox from 'Components/reportBox/reportBox.js';
import { postReport } from 'Redux/actions/reportactions.js';
import jumpApi from 'Config/jumpApi.js';
import moment from 'moment';
const Userlabel = styled.div`
   height:39px;
   padding:6px 20px;
   font-size:13px;
   color:#54C3D4 ;
   background:#EEF8FB ;
   border-radius:31px;
   margin:4px 6px;
   line-height:25px;
   display:flex;
   justify-content:start;
   align-items:center;
`
const Userlittlereport = styled.div`
cursor: pointer;
transition: all 0.3s;
&:hover{
    color:#54C3D4;
}
`
@connect(({ works: { worksDetail, recommendWorks, commentList }, login: { token } }) => ({ worksDetail, token, recommendWorks, commentList }))
class Worksdetail extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            commentPageNumber: 0,
            commentPageSize: 10,
            replyPageNumber: 0,
            replyPageSize: 10,
            showMore: false,
            reportType: 1,
            defendantId: '',
            canSee: false,
            commentVisible: false,
            replyVisible: false,
            deleteCommentId: '',
            deleteCommentPageTotal: 0,
            deleteReplyId: '',
        }
    }
    componentDidMount() {
        const { dispatch, match } = this.props
        const { commentPageNumber, commentPageSize } = this.state;
        dispatch(getWorkDetail({ productionId: match.params.id, lookCount4AddFlag: 2 }))
        dispatch(getCommentList({ productionId: match.params.id, pageNumber: commentPageNumber, pageSize: commentPageSize }))
        dispatch(getWorkList({ type: '2', pageNumber: '0', pageSize: '10' }))
    }
    adaptedWorks = (id) => {
        window.location.href = `${jumpApi}?type=3&productionId=${id}`
    }
    doLike = (id) => {
        const { token, dispatch, worksDetail = {} } = this.props;
        const { likeFlag } = worksDetail;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        likeFlag === 2 ? dispatch(cancelLike({ productionId: id })) : dispatch(doLike({ productionId: id }))
    }
    hasCollected = (id) => {
        const { token, dispatch, worksDetail = {} } = this.props;
        const { collectFlag } = worksDetail;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        collectFlag === 2 ? dispatch(cancelCollected({ productionId: id })) : dispatch(doCollected({ productionId: id }))
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    }
    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }
    reportSubmit = (submit) => {
        const { dispatch } = this.props;
        dispatch(postReport(submit))
        this.setState({
            visible: false,
        });
    }
    onClickForMoreReply = (id, replyPageNumber, replyPageSize) => {
        const { dispatch } = this.props;
        dispatch(getReplyList({ commentId: id, pageNumber: replyPageNumber + 1, pageSize: replyPageSize }))
        this.setState({ replyPageNumber: replyPageNumber + 1, replyPageSize })
    }
    onChangePage = (page) => {
        const { dispatch, match } = this.props;
        const { commentPageSize } = this.state;
        const data = {
            productionId: match.params.id,
            pageNumber: page - 1,
            pageSize: commentPageSize
        }
        dispatch(getCommentList(data))
        this.setState({ commentPageNumber: page - 1 })
    }
    onClickShowMore = () => {
        this.setState({ showMore: !this.state.showMore })
    }
    commentSubmit = (id, content) => {
        const { token, dispatch } = this.props;
        const { commentPageSize, commentPageNumber } = this.state;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        const data = {
            productionId: id,
            pageNumber: commentPageNumber,
            pageSize: commentPageSize,
            content
        }
        dispatch(postComment(data))
    }
    replySubmit = (id, content, targetId, type) => {
        const { token, dispatch, match } = this.props;
        const { commentPageSize, commentPageNumber } = this.state;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        const data = {
            productionId: match.params.id,
            pageNumber: commentPageNumber,
            pageSize: commentPageSize,
            content, type,
            commentId: id,
            targetId
        }
        dispatch(postReply(data))
    }
    likeComment = (likeFlag, commentId) => {
        const { token, dispatch, match } = this.props;
        const { commentPageSize, commentPageNumber } = this.state;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        const data = {
            productionId: match.params.id,
            pageNumber: commentPageNumber,
            pageSize: commentPageSize,
            likeFlag,
            commentId
        }
        likeFlag === 2 ? dispatch(cancelCommentLike(data)) : dispatch(doCommentLike(data))
    }
    likeReply = (likeFlag, replyId, commentId) => {
        const { token, dispatch, match } = this.props;
        const { commentPageSize, commentPageNumber } = this.state;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        const data = {
            productionId: match.params.id,
            pageNumber: commentPageNumber,
            pageSize: commentPageSize,
            likeFlag,
            replyId,
            commentId
        }
        likeFlag === 2 ? dispatch(cancelReplyLike(data)) : dispatch(doReplyLike(data))
    }
    worksOrCommentOrReplyReport = (reportType, defendantId) => {
        const { token, dispatch } = this.props;
        this.setState({ canSee: false })
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        this.setState({ reportType, defendantId, visible: true })
    }
    deleteComment = (deleteCommentId, deleteCommentPageTotal) => {
        const { token, dispatch } = this.props;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        this.setState({ deleteCommentId, deleteCommentPageTotal, commentVisible: true })
    }
    commentHandleOk = () => {
        const { dispatch, match } = this.props;
        const { commentPageSize, commentPageNumber, deleteCommentId, deleteCommentPageTotal } = this.state;
        const parseIntPageNumber = deleteCommentPageTotal > 10 && ((deleteCommentPageTotal - 1) / commentPageSize === commentPageNumber) ?
            (commentPageNumber - 1) : commentPageNumber;
        const data = {
            productionId: match.params.id,
            pageNumber: parseIntPageNumber,
            pageSize: commentPageSize,
            commentId: deleteCommentId
        }
        dispatch(deleteComment(data))
        this.setState({ commentPageNumber: parseIntPageNumber, commentVisible: false })
    }
    deleteReply = (deleteReplyId) => {
        const { token, dispatch } = this.props;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        this.setState({ deleteReplyId, replyVisible: true })
    }
    replyHandleOk = () => {
        const { dispatch, match } = this.props;
        const { commentPageSize, commentPageNumber, deleteReplyId } = this.state;
        const data = {
            productionId: match.params.id,
            pageNumber: commentPageNumber,
            pageSize: commentPageSize,
            replyId: deleteReplyId
        }
        dispatch(deleteReply(data))
        this.setState({ replyVisible: false })
    }
    handleVisibleChange = visible => {
        this.setState({ canSee: visible });
    }
    render() {
        const { worksDetail = {}, recommendWorks, commentList } = this.props;
        const { visible, commentPageSize, commentPageNumber, replyPageSize, replyPageNumber,
            showMore, reportType, defendantId, canSee, commentVisible, replyVisible } = this.state;
        const { productionTagInfoList = [], likeFlag, collectFlag, likeCount, content, id, openFlag, selfFlag, state } = worksDetail;
        const { content: recommendWorksList = [] } = recommendWorks;
        const { content: myCommentList = [], pageable: commentPageable = {} } = commentList;
        const { totalSize: commentTotalSize = 0 } = commentPageable;
        return (
            <div className='hole-card'>
                <Row className='works-detail'>
                    <Row type='flex' justify='start' className='parent'> <div className='stable' >
                        <Row className='works-detail-row1'>{worksDetail.name}</Row>
                        <Row type='flex' justify='start' align='middle' className='works-detail-row2'>
                            <Col><img style={{ width: '26px', height: '26px', borderRadius: '26px' }} src={worksDetail.authorAvatar} alt='' /></Col>
                            <Col style={{ marginLeft: '8px' }}>{worksDetail.authorName}</Col>
                            <Col style={{ color: '#999999', marginLeft: '14px' }}>{`更新时间：\xa0${moment(worksDetail.updatedAt).format("YYYY-MM-DD")}`}</Col>
                        </Row>
                        {
                            state === 3 ? <div className='works-offline'>
                                <div className='works-offline-content'>
                                    <img
                                        style={{ width: '100px' }}
                                        src={`${process.env.PUBLIC_URL}/assets/works-offline.png`}
                                        alt='' />
                                    <div style={{ padding: '10px 0' }}>该作品违反平台规定，已被管理员下线</div>
                                </div>
                            </div>
                                : content && <iframe
                                    className='works-detail-row3'
                                    title='works-detail'
                                    seamless='seamless'
                                    frameBorder='0'
                                    src={`${jumpApi}?type=1&url=${content}`} >
                                </iframe>
                        }
                    </div><div className='change' >
                            <Row> <Row type='flex' justify='end'>
                                <Col className='works-star'> <Col style={{ fontSize: '36px', paddingRight: '10px', color: '#FFDC17' }}>
                                    {worksDetail.score ? (worksDetail.score / 10).toFixed(1) : '0.0'}</Col>
                                    <Rate style={{ fontSize: '16px' }} disabled allowHalf value={worksDetail.score ? worksDetail.score / 20 : 0} />
                                </Col>
                            </Row>
                                <div className='col-overflow'>
                                    <Row className='col-row1'>作品介绍：</Row>
                                    <Row className='col-row2'>{worksDetail.intro}</Row>
                                    <Row className='col-row1'>操作介绍</Row>
                                    <Row className='col-row2'>{worksDetail.operateExplain}</Row>
                                </div>
                                <Row className='col-row3'>{
                                    productionTagInfoList && productionTagInfoList.map((item) => {
                                        return <div key={item.id}><Userlabel>
                                            <img style={{ width: '18px', height: '14px', marginRight: '6px' }} src={item.imgUrl} alt='' />
                                            {item.name}
                                        </Userlabel>
                                        </div>
                                    })}</Row>
                            </Row>
                            <Row type='flex' justify='start' align='middle' className='col-row4'>
                                {<Button
                                    disabled={state === 3 || ((selfFlag === 1) && (openFlag === 2))}
                                    onClick={this.adaptedWorks.bind(this, worksDetail.id)}
                                    style={{ width: '328px', height: '52px', borderRadius: '52px' }}
                                    type='primary'>{selfFlag === 1 ? '改编作品' : '编辑作品'}</Button>}</Row>
                        </div>
                    </Row>
                    <Row type='flex' justify='start' align='middle' className='works-detail-row4'>
                        <Col className='user-margin'>
                            <Icon style={{ fontSize: '20px', color: '#CDCFD7', marginRight: '6px' }} type="eye" theme="filled" />
                            {worksDetail.lookCount}
                        </Col>
                        <Col className='user-margin'>
                            <Icon
                                onClick={this.doLike.bind(this, worksDetail.id)}
                                style={{ fontSize: '20px', color: likeFlag === 2 ? '#FFBF00' : '#CDCFD7', marginRight: '6px', cursor: 'pointer' }}
                                type="like"
                                theme="filled" />
                            {likeCount}
                        </Col>
                        <Col className='user-margin'>
                            <Icon
                                onClick={this.hasCollected.bind(this, worksDetail.id)}
                                style={{ fontSize: '20px', color: collectFlag === 2 ? '#FFBF00' : '#CDCFD7', marginRight: '6px', cursor: 'pointer' }}
                                type="star" theme="filled" />
                            {collectFlag === 2 ? '已收藏' : '未收藏'}
                        </Col>
                        <Col className='row4-col'>
                            <Popover placement="bottom"
                                visible={canSee}
                                onVisibleChange={this.handleVisibleChange}
                                content={<Userlittlereport
                                    onClick={this.worksOrCommentOrReplyReport.bind(this, 1, worksDetail.id)}>举报</Userlittlereport>}
                                trigger="click">
                                <div className='row4-col-div'>
                                    <img className='row4-col-div-img' src={`${process.env.PUBLIC_URL}/assets/icon-more-green.png`} alt='' />
                                    <div style={{ cursor: 'pointer' }}>更多</div>
                                </div>
                            </Popover></Col>
                    </Row>
                </Row>
                <Row className='works-detail-row5' type='flex' justify='start' >
                    <div className='works-detail-row5-stable'><Card style={{ borderRadius: '10px' }}>
                        <Row className='row-comment' type='flex' justify='start' align='middle'>
                            <div className='row-comment-div'>评论</div>
                            <div>{`（${commentTotalSize}）`}</div>
                        </Row>
                        <Row><Commentbox id={id} onClickSubmit={this.commentSubmit.bind(this)} /></Row>
                        {
                            myCommentList && myCommentList.length > 0 && <Row style={{ paddingBottom: '45px' }}>
                                {
                                    myCommentList.map(item => {
                                        const productionCommentReplyInfoList = (item.productionCommentReplyInfoList && item.productionCommentReplyInfoList.length > 0) ?
                                            (showMore ? item.productionCommentReplyInfoList : item.productionCommentReplyInfoList.slice(0, 3)) : []
                                        return <div key={item.id}
                                            style={{ width: '100%', borderBottom: '1px solid #F0F0F0', paddingBottom: '24px' }}>
                                            <Commentitem
                                                total={commentTotalSize}
                                                deleteCommentOrReply={this.deleteComment.bind(this)}
                                                worksOrCommentOrReplyReport={this.worksOrCommentOrReplyReport.bind(this)}
                                                reportType={2}
                                                likeCommentOrReply={this.likeComment.bind(this)}
                                                data={item}
                                                commentId={item.id}
                                                replySubmit={this.replySubmit.bind(this)}
                                                type={1}
                                                targetId={item.id} />
                                            {productionCommentReplyInfoList.length > 0 &&
                                                <div style={{ maxHeight: '1500px', overflow: 'auto' }}>
                                                    {
                                                        productionCommentReplyInfoList.map(itema => {
                                                            return <div key={`${itema.id}${itema.createdAt}`} className='reply'>
                                                                <div style={{ borderBottom: '1px solid #F0F0F0', margin: '0 24px' }}>
                                                                    <Commentitem
                                                                        total={commentTotalSize}
                                                                        deleteCommentOrReply={this.deleteReply.bind(this)}
                                                                        worksOrCommentOrReplyReport={this.worksOrCommentOrReplyReport.bind(this)}
                                                                        reportType={3}
                                                                        likeCommentOrReply={this.likeReply.bind(this)}
                                                                        data={itema}
                                                                        commentId={item.id}
                                                                        replySubmit={this.replySubmit.bind(this)}
                                                                        type={2}
                                                                        targetId={itema.id} />
                                                                </div>
                                                            </div>
                                                        })}
                                                    <div className='comment-footer'>
                                                        {showMore ? (replyPageNumber + 1 < Math.ceil(item.productionCommentReplyCount / replyPageSize)) &&
                                                            <div
                                                                onClick={this.onClickForMoreReply.bind(this, item.id, replyPageNumber, replyPageSize)}
                                                                style={{ color: '#54C3D4' }} >加载更多</div> :
                                                            `共${item.productionCommentReplyCount}条回复`}
                                                        {item.productionCommentReplyCount > 3 && <div onClick={this.onClickShowMore}
                                                            style={{ paddingLeft: '10px', color: '#54C3D4' }}>
                                                            {showMore ? <div>收起<Icon type="up" /></div> : <div>展开<Icon type="down" /></div>}
                                                        </div>}
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                    })
                                }
                                <div className='notification-content-page'>
                                    <Pagination
                                        defaultPageSize={10}
                                        current={commentPageNumber + 1}
                                        pageSize={commentPageSize}
                                        onChange={this.onChangePage}
                                        hideOnSinglePage={true}
                                        defaultCurrent={1}
                                        total={commentTotalSize} />
                                </div>
                            </Row>}
                    </Card></div>
                    <div className='works-detail-row5-change'><div style={{ background: '#fff', borderRadius: '10px' }}>
                        <Row type='flex' justify='center' align='middle'
                            style={{ fontSize: '18px', fontWeight: 'bold', height: '60px', marginBottom: '15px', borderBottom: '1px solid #DCDCDC' }}>推荐作品</Row>
                        <Row>{recommendWorksList.map((item, index) => {
                            return <div key={index}><Recommenditem itemData={item} /></div>
                        })}</Row></div></div>
                </Row>
                <Modal
                    footer={null}
                    visible={visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    bodyStyle={{ padding: '24px 28px' }}
                    destroyOnClose={true}
                >
                    <ReportBox reportSubmit={this.reportSubmit.bind(this)} defendantId={defendantId} reportType={reportType} />
                </Modal>
                <Modal
                    width={'386px'}
                    bodyStyle={{ width: '100%', padding: '24px 50px 30px 50px' }}
                    visible={commentVisible}
                    onCancel={() => { this.setState({ commentVisible: false }) }}
                    footer={null}
                    destroyOnClose
                    closable={false}
                >
                    <div className='delete-modal-title'>提醒</div>
                    <div className='delete-modal-content'>{`确定要删除该评论吗？`}</div>
                    <div className='delete-modal-parent'>
                        <Button className='delete-modal-parent-button' onClick={() => { this.setState({ commentVisible: false }) }}>取消</Button>
                        <Button className='delete-modal-parent-button' onClick={this.commentHandleOk} type='primary'>删除</Button>
                    </div>
                </Modal>
                <Modal
                    width={'386px'}
                    bodyStyle={{ width: '100%', padding: '24px 50px 30px 50px' }}
                    visible={replyVisible}
                    onCancel={() => { this.setState({ replyVisible: false }) }}
                    footer={null}
                    destroyOnClose
                    closable={false}
                >
                    <div className='delete-modal-title'>提醒</div>
                    <div className='delete-modal-content'>{`确定要删除该回复吗？`}</div>
                    <div className='delete-modal-parent'>
                        <Button className='delete-modal-parent-button' onClick={() => { this.setState({ replyVisible: false }) }}>取消</Button>
                        <Button className='delete-modal-parent-button' onClick={this.replyHandleOk} type='primary'>删除</Button>
                    </div>
                </Modal>
            </div>
        )
    }
}
export default Worksdetail;
const Recommenditem = React.memo(
    withRouter(({ itemData = {}, history }) => {
        const { name = '' } = itemData;
        const toWorksDetail = () => {
            history.push(`/works-show/${itemData.id}`)
        }
        return <div className='recommend-item'>
            <img src={itemData.coverUrl} onClick={toWorksDetail} className='recommend-item-left' alt='' />
            <div className='recommend-item-right'
            >
                <div style={{ color: '#444444' }}>{name.length > 9 ? `${name.substr(0, 9)}...` : name}</div>
                <div style={{ display: 'flex', alignItems: 'center', height: '20px', color: '#999999' }}>
                    <div className='recommend-item-right-title'>{itemData.score ? (itemData.score / 10).toFixed(1) : '0.0'}</div>
                    <Rate style={{ fontSize: '11px' }} allowHalf disabled value={itemData.score ? itemData.score / 20 : 0} />
                </div>
            </div>
        </div>
    })

)