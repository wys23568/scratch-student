import React from 'react';
import { Button, Carousel, Icon, BackTop, Modal, Select } from 'antd';
import './homepage.less';
import { connect } from 'react-redux';
import { showLoginModal, weixinLogin } from 'Redux/actions/loginactions.js';
import { getHomepageInfo } from 'Redux/actions/homepageactions.js';
import routerApi from 'Config/routerApi.js';
import * as message from 'Message';
import moment from 'moment';
const { Option } = Select;
@connect(({ homepage: { homepageInfo }, login: { token } }) => ({ homepageInfo, token }))
class Homepage extends React.PureComponent {
    constructor() {
        super()
        this.state = {
            token: '',
            visible: false,
            lessonInfo: {}
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const GetQueryString = (name) => {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return (r[2]); return null;
        }
        if (GetQueryString('code')) {
            dispatch(weixinLogin(GetQueryString('code')))
        }
        dispatch(getHomepageInfo())
    }
    componentDidUpdate(prevProps, prevState) {
        if (!prevProps.token && this.props.token) {
            window.location.href = routerApi;
        }
    }
    onClickToPay = (course) => {
        const { token, dispatch } = this.props;
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        else if (!course) {
            message.info('课程未开放购买~')
            return
        }
        else if (course.buyFlag === 2) {
            message.info('您已购买过该课程，请不要重复购买呢~')
            return
        }
        this.setState({ visible: true })
    }

    handleCancel = e => {
        this.setState({
            visible: false,
            lessonInfo: {}
        });
    };
    onChangeLesson = (value) => {
        const { homepageInfo } = this.props;
        const { coursePackageInfoList = [] } = homepageInfo;
        let lessonInfo;
        coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList.forEach(item => {
            if (item.id === value.key) {
                lessonInfo = item
            }
        })
        this.setState({ lessonInfo })

    }
    onClickHasChoose = () => {
        const { lessonInfo } = this.state;
        const { history } = this.props;
        if (!lessonInfo.issue) {
            message.info('请您先选择课程期次呢~')
            return
        }
        this.setState({ visible: false })
        history.push(`/order-payment/${lessonInfo.id}`)

    }
    render() {
        const { homepageInfo } = this.props;
        const { coursePackageInfoList = [], teacherInfoList = [], qrCodeUrl4Official = '' } = homepageInfo;
        const { visible, lessonInfo } = this.state;
        const whyLearn = [{ id: 'one', content: '培养孩子的专注力，提升各科学习的效率' },
        { id: 'two', content: '强化孩子逻辑思维能力的有效方法' },
        { id: 'three', content: '让孩子的创造力用最贴近时代的方式展现' }];
        const whyHaimawangPic = [{ id: 1, height: '73px' },
        { id: 2, height: '84px' },
        { id: 3, height: '105px' },
        { id: 4, height: '133px' },
        { id: 5, height: '155px' },
        { id: 6, height: '175px' }];
        const whyHaimawangWord = [{ id: 1, background: '#54C2FB', content: '熟悉界面和流程培养编程兴趣' },
        { id: 2, background: '#54C2FB', content: '多样化编程效果实现提升自主纠错水平和逻辑思维能力' },
        { id: 3, background: '#7CD702', content: '不同语句综合应用理解分支程序之间的区别和联系' },
        { id: 4, background: '#7CD702', content: '程序自主创作提升制作多角色、多背景的复杂流程程序' },
        { id: 5, background: '#FFBD00', content: '解锁列表、函数等高级模块应用编程和数字艺术相结合' },
        { id: 6, background: '#FFBD00', content: '多学科融合进行编程项目创作，培养孩子解决问题能力' }];
        const notOnlyProgram = [{ id: 1, content: '天文地理' }, { id: 2, content: '创意动画' }, { id: 3, content: '数理化生' }, { id: 4, content: '逻辑流程' }]
        return (
            <div className='homepage'>
                <BackTop>
                    <img className='harvest-top' src={`${process.env.PUBLIC_URL}/assets/icon-to-top.png`} alt='' />
                </BackTop>
                <div className='homepagebigpicture'
                    style={{
                        background: `url(${process.env.PUBLIC_URL}/assets/picture-background.png) no-repeat top`,
                        backgroundSize: 'auto 550px'
                    }}>
                    <div className='homepage-big-picture'>
                        <div className='homepage-big-picture-left'>
                            <img src={`${process.env.PUBLIC_URL}/assets/picture-background-word.png`} style={{ width: '389px' }} alt='' />
                        </div>
                        <div className='homepage-big-picture-right'>
                            <div className='homepage-big-picture-right-a'>编程体验课</div>
                            <div className='homepage-big-picture-right-b'>{`原价${coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0].price}元`}</div>
                            <div className='homepage-big-picture-right-c'>{coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0].realPrice}<span
                                style={{ fontSize: '25px' }}>元</span><span style={{ fontSize: '25px', color: '#fff' }}>/6节课</span></div>
                            <div className='homepage-big-picture-right-d'>(开课前无条件退款)</div>
                            <Button
                                className='homepage-big-picture-right-button'
                                type='primary'
                                onClick={this.onClickToPay.bind(this, coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0])}
                            >课程购买</Button>
                        </div>
                    </div>
                </div>
                <div className='whylearn'>
                    <div className='why-learn'>
                        <Titlestyle small={''} big={'为什么学编程'} />
                        <div className='why-learn-pictur'>
                            {
                                whyLearn.map(item => {
                                    return <div className='why-learn-pictur-item' key={item.id}>
                                        <img src={`${process.env.PUBLIC_URL}/assets/why-learn-${item.id}.png`} className='why-learn-pictur-item-pic' alt='' />
                                        <div className='why-learn-pictur-item-word'>{item.content}</div>
                                    </div>
                                })
                            }
                        </div>
                    </div>
                </div>
                <div className='whyhaimawang'>
                    <div className='why-haimawang'>
                        <Titlestyle small={''} big={'为什么在海码王学编程'} />
                        <div style={{width:'100%',height:'80px'}}></div>
                        <div className='why-haimawang-tags'>
                            <div className='why-haimawang-tags-row'>
                                <div className='why-haimawang-tags-row-one' style={{ height: '56px', marginRight: '1px' }}>课程阶段</div>
                                {whyHaimawangPic.map(item => {
                                    return <img
                                        className='why-haimawang-tags-row-one'
                                        style={{ width: '126px', height: item.height }}
                                        key={item.id}
                                        src={`${process.env.PUBLIC_URL}/assets/why-haimawang-lv${item.id}.png`}
                                        alt='' />
                                })}
                            </div>
                            <div className='why-haimawang-tags-row'>
                                <div className='why-haimawang-tags-row-one' style={{ height: '89px' }}>参考<br />CSTA标准</div>
                                <div className='why-haimawang-tags-row-m' style={{ background: '#54C2FB' }}>{`Level\xa01A-1B`}</div>
                                <div className='why-haimawang-tags-row-m' style={{ background: '#7CD702' }}>{`Level\xa01A-1B`}</div>
                                <div className='why-haimawang-tags-row-m' style={{ background: '#FFBD00' }}>{`Level\xa01A-1B`}</div>
                            </div>
                            <div className='why-haimawang-tags-row'>
                                <div className='why-haimawang-tags-row-one' style={{ height: '188px' }}>课程内容</div>
                                {
                                    whyHaimawangWord.map(item => {
                                        return <div key={item.id} className='why-haimawang-tags-row-b' style={{ background: item.background }}>{item.content}</div>
                                    })
                                }
                            </div>
                        </div>
                        <div className='why-haimawang-content'>{`资深教研倾情打造\xa0\xa0适合6-12岁孩子学习的课程体系`}</div>
                    </div>
                </div>
                <div className='whatteacher'>
                    <div className='what-teacher'>
                        <Titlestyle small={''} big={'专业的教研教学阵容打造优质的课程品质'} />
                        <div className='what-teacher-parent'>
                            <Icon
                                onClick={() => { this.carouselRef.prev() }}
                                style={{ fontSize: '45px', color: '#C8CCD0' }}
                                type="left" />
                            <div className='what-teacher-carousel'>
                                <Carousel
                                    ref={el => this.carouselRef = el}
                                    autoplay
                                    slidesToShow={2}
                                    dots={false}>
                                    {
                                        teacherInfoList.map(item => {
                                            return <div key={item.id} >
                                                <div className='what-teacher-carousel-item'>
                                                    <img className='what-teacher-carousel-item-img' src={item.avatarUrl} alt='' />
                                                    <div style={{ color: '#555555', fontSize: '24px' }}>{item.name}</div>
                                                    <div style={{ color: '#888888', fontSize: '18px', paddingTop: '6px' }}>{`特邀授课老师`}</div>
                                                    {<div className='what-teacher-carousel-item-intro'>
                                                        <p>老师介绍</p>
                                                        <div>{item.info}</div>
                                                    </div>}
                                                </div>
                                            </div>
                                        })
                                    }
                                </Carousel>
                            </div>
                            <Icon
                                onClick={() => { this.carouselRef.next() }}
                                style={{ fontSize: '45px', color: '#C8CCD0' }}
                                type="right" />
                        </div>
                    </div>
                </div>
                <div className='studyonline'>
                    <div className='study-online'>
                        <Titlestyle small={''} big={'专业教学老师在线辅导，在家学习质量也有保障'} />

                        <div  style={{width:'100%',height:'60px'}}></div>
                        <div className='study-online-pic'>
                            <div>
                                <img
                                    className='study-online-pic-item'
                                    src={`${process.env.PUBLIC_URL}/assets/study-online2.png`} alt='' />
                                <div className='study-online-word' style={{ paddingTop: '20px' }}>孩子的每一份作品，都会被悉心点评</div>
                            </div>
                            <div>
                                <img
                                    className='study-online-pic-item'
                                    src={`${process.env.PUBLIC_URL}/assets/study-online1.png`} alt='' />
                                <div className='study-online-word' style={{ paddingTop: '20px' }}>孩子的每一个疑问，都将及时得到解答</div>
                            </div>
                        </div>
                    </div></div>
                <div className='trylesson'>
                    <div className='try-lesson'>
                        <Titlestyle small={''} big={'试听课的安排'} />
                        <div className='try-lesson-pic'>
                            <img
                                style={{ width: '941px'}}
                                src={`${process.env.PUBLIC_URL}/assets/try-lesson.png`} alt='' />
                            <div style={{ width: '941px', display: 'flex', justifyContent: 'center', paddingTop: '30px' }}>
                                <img className='try-lesson-button' src={`${process.env.PUBLIC_URL}/assets/sign-up-1.png`}
                                    onClick={this.onClickToPay.bind(this, coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0])} alt='' />
                            </div>
                        </div>
                    </div>
                </div>
                <div className='notonlyprogram'>
                    <div className='not-only-program'>
                        <Titlestyle small={''} big={'在海码王学编程，不仅仅是编程～'} />
                        <div className='not-only-program-pic'>
                            <img style={{ width: '324px' }}
                                src={`${process.env.PUBLIC_URL}/assets/not-only-program-code.png`} alt='' />
                            <img
                                style={{ width: '540px' }}
                                src={`${process.env.PUBLIC_URL}/assets/not-only-program-marry.png`} alt='' />
                        </div>
                        <div className='not-only-program-list'>
                            {
                                notOnlyProgram.map(item => {
                                    return <div key={item.id} className='not-only-program-item'>
                                        <img
                                            className='not-only-program-item-pic'
                                            src={`${process.env.PUBLIC_URL}/assets/not-only-program-${item.id}.png`} alt='' />
                                        <div className='not-only-program-item-word'>{item.content}</div>
                                    </div>
                                })
                            }
                        </div>
                    </div>
                </div>
                <div className='harvestparent'>
                    <div className='harvest'>
                        <Titlestyle small={''} big={'孩子上完一个课程，就能有满满的收获'} />
                        <img
                            style={{ width: '1000px', margin: '60px 0' }}
                            src={`${process.env.PUBLIC_URL}/assets/harvest-bkgr.png`} alt='' />
                        <div>
                            <img className='harvest-button' src={`${process.env.PUBLIC_URL}/assets/sign-up-2.png`}
                                onClick={this.onClickToPay.bind(this, coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0])} alt='' />
                        </div>

                    </div>
                </div>
                <div style={{
                    width: '100%',
                    background: `url(${process.env.PUBLIC_URL}/assets/background-bottom.png) no-repeat top`,
                    backgroundSize: 'auto 480px'
                }}>
                    <div className='commonproblem'>
                        <div className='common-problem'>
                            <div className='common-problem-title' ><Icon type="question-circle" />{`\xa0常见问题`}</div>
                            <div className='common-problem-content' >
                                <div style={{ width: '465px' }}>
                                    <div> 问题一：怎么上课？</div>
                                    <div> 回答：在家使用电脑即可。建议下载谷歌浏览器，提升创作体验。</div>
                                    <br />
                                    <br />
                                    <div>问题二：目前有免费试听课吗？</div>
                                    <div>回答：欢迎家长关注“海码王编程训练营”微信公众号，我们不定期会推出优惠福利活动。</div>
                                    <br />
                                    <br />
                                    <div> 问题三：家长需要参与课程吗？</div>
                                    <div>回答：前1-2次课程，家长需要配合给孩子准备一下电脑浏览器。孩子的学习账号需要家长授权登录。</div>
                                </div>
                                <div style={{ width: '465px', display: 'flex', justifyContent: 'center', alignItems: 'flex-end', flexDirection: 'column' }}>
                                    <div style={{ display: 'flex', justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center' }}>
                                        <img src={qrCodeUrl4Official} style={{ width: '175px', height: '175px', background: '#fff' }} alt='' />
                                        <div style={{ textAlign: 'center', fontSize: '13px', paddingTop: '10px' }}>
                                            更多问题咨询，关注海码王服务号
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='footer'>
                        <div>海码王学生版、海码王教师版</div>
                        <div>Copyright © 2019 北京前海互联科技有限公司版权所有. 京ICP备19024282号-1</div>
                        <div>服务热线：400-800-8540</div>
                    </div>
                </div>
                <Modal
                    visible={visible}
                    footer={null}
                    onCancel={this.handleCancel}
                    destroyOnClose
                    closable={false}
                >
                    <div className='modal-homepage-content'>
                        <div className='modal-homepage-content-title'>请选择孩子的上课时间</div>
                        <div className='modal-homepage-content-item'>
                            <div className='modal-homepage-content-item-left'>课程名称：</div>
                            <div style={{ width: '268px', padding: '4px 11px', borderRadius: '5px', border: '1px solid #54C3D4' }}>
                                {coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0].name}</div>
                        </div>
                        <div className='modal-homepage-content-item'>
                            <div className='modal-homepage-content-item-left'>课程期次：</div>
                            <Select
                                style={{ width: '268px' }}
                                dropdownStyle={{ maxHeight: '120px', overflow: 'auto' }}
                                showSearch
                                placeholder="请选择课程期次"
                                optionFilterProp="children"
                                onChange={this.onChangeLesson}
                                labelInValue
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {
                                    coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList.map(item => {
                                        return <Option key={item.id} value={item.id}>{`${item.issue}期`}</Option>
                                    })
                                }
                            </Select>
                        </div>
                        <div className='modal-homepage-content-item'>
                            <div className='modal-homepage-content-item-left'>课程辅导时间：</div>
                            <div style={{ width: '268px', height: '31px', padding: '4px 11px', borderRadius: '5px', border: '1px solid #54C3D4' }}>
                                {lessonInfo.teachStartAt &&
                                    `${moment(lessonInfo.teachStartAt).format('YYYY年MM月DD日')}~${moment(lessonInfo.teachEndAt).format('YYYY年MM月DD日')}`}
                            </div>
                        </div>
                        <div style={{ width: '100%', padding: '6px 0 0 139px', fontSize: '12px', color: '#999999' }}>在线辅导时间：周一至周五 9:00 - 22:00 </div>
                        <Button
                            onClick={this.onClickHasChoose}
                            className='modal-homepage-content-course-button'
                            type='primary'>选好了</Button>
                    </div>
                </Modal>
            </div>
        )
    }
}
export default Homepage;
const Titlestyle = React.memo(
    ({ small, big, back }) => {
        return <div className='title-style'>{back}
            <div className='for-ios'>
                <div className='title-style-position'>
                    <div className='ball-s' />
                    <div className='ball-l' />
                    <div className='ball-m' />
                    <div className='title-style-position-content'>{small}<div className='title-style-position-big'>{big}</div></div>
                    <div className='ball-m' />
                    <div className='ball-l' />
                    <div className='ball-s' />
                </div>
            </div>
        </div>
    }
)