import React, { PureComponent } from 'react';
import './homeworkDetail.less';
import { Card, Row, Button } from 'antd';
import { getHomeworkDetail } from 'Redux/actions/homeworkactions.js';
import { connect } from 'react-redux';
import moment from 'moment';
import jumpApi from 'Config/jumpApi.js';
@connect(({ homework: { homeworkDetail } }) => ({ homeworkDetail }))
class Homeworkdetail extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
        const { dispatch, location } = this.props;
        let params = new URLSearchParams(location.search);
        dispatch(getHomeworkDetail({
            courseId: params.get('courseId'),
            courseLessonId: params.get('courseLessonId'),
            teacherCourseId: params.get('teacherCourseId')
        }))

    }
    toUpdateHomework = (courseId, courseLessonId, teacherCourseId) => {
        window.location.href = `${jumpApi}?type=2&courseId=${courseId}&courseLessonId=${courseLessonId}&teacherCourseId=${teacherCourseId}`
    }
    render() {
        const { homeworkDetail } = this.props;
        const { courseLessonNumber, courseLessonTitle, updatedAt, comment, commitAt, courseId, teacherCourseId, content, draft, courseLessonId } = homeworkDetail;
        return (
            <div className='homeworks-detail'>
                <Card style={{ borderRadius: '10px' }}>
                    <Row className='detail-title'>{courseLessonNumber && `第${courseLessonNumber}课时——${courseLessonTitle}`}</Row>
                    <Row className='detail-time'>{`更新时间：\xa0${moment(updatedAt).format("YYYY-MM-DD")}`}</Row>
                    <div className='parent'>
                        <div className='stable'>
                            {(draft || content) && <iframe
                                className='works-detail-row3'
                                title='works-detail'
                                seamless='seamless'
                                frameBorder='0'
                                src={`${jumpApi}?type=1&url=${draft || content}`} >
                            </iframe>}
                        </div>
                        <div className='change'>
                            <div className='change-title'>老师评语</div>
                            {comment && <div className='change-comment-time'>{`评语时间：\xa0${moment(commitAt).format("YYYY-MM-DD")}`}</div>}
                            <div className='change-content'>
                                {
                                    comment ? comment : <div className='comment-blank'>
                                        <img style={{ width: '65px' }} src={`${process.env.PUBLIC_URL}/assets/nocomment.png`} alt='' />
                                        <div className='comment-blank-font'>
                                            <div>请耐心等待一下哦</div>
                                            <div>老师的评语正在飞奔的路上～</div>
                                        </div>
                                    </div>
                                }
                            </div>
                            <Button
                                onClick={this.toUpdateHomework.bind(this, courseId, courseLessonId, teacherCourseId)}
                                className='change-button'
                                type='primary'
                            >更新作业</Button>
                        </div>
                    </div>
                </Card>
            </div>
        )
    }
}
export default Homeworkdetail;