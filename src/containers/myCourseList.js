import React, { PureComponent } from 'react';
import { Row, Card, Col, Button, Icon, Modal, Rate } from 'antd';
import * as message from 'Message';
import styled from 'styled-components';
import jumpApi from 'Config/jumpApi.js';
import './myCourseList.less';
import { getCourseList, closeShowGradeBox, postGradeForTeacher, openShowGradeBox } from 'Redux/actions/courseactions.js';
import { connect } from 'react-redux';
import moment from 'moment';
import Myvideo from 'Components/Myvideo/Myvideo.js';
import Orderdistribution from 'Components/orderDistribution/orderDistribution.js';
export const Scratch = styled.div`
height: 28px;
color:#555555;
display:flex;
font-size:16px;
align-items:center;
`
const Userlistitem = styled.div`
border-radius: 10px;
border:1px solid #efefef;
width:275px;
position:relative;
transition:all 0.3s;
transform:${props => props.hasChoose && 'scale(1.05)'};
box-shadow:${props => props.hasChoose && '2px 3px 16px 1px rgba(4, 0, 0, 0.4)'};
&:hover{
    box-shadow: 2px 3px 16px 1px rgba(4, 0, 0, 0.4);
}
`
@connect(({ course: { courseList, showGradeBox, courseDistribution } }) => ({ courseList, showGradeBox, courseDistribution }))
class MyCourseList extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                courseId: '',
                id: '',
                title: '',
                knowledges: '',
                studentCourseHomeworkInfo: { studyFlag: '' },
                coverUrl: '',
                videoUrl: '',
                lockFlag: 2,
                teacherCourseId: '',
                number: '',
                startAt: ''
            },
            visible: false,
            rateValue: 5,
            showCartoon: false,
            showCartoonBox: false
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getCourseList({ pageNumber: '0', pageSize: '20' }));
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (!prevState.data.courseId && nextProps.courseList && (nextProps.courseList.length !== 0)) {
            return {
                ...prevState,
                data: nextProps.courseList[0].courseLessonInfoList[0]
            }
        }
        return null
    }
    courseClick = (data, studentScore, stopFlag) => {
        const { dispatch } = this.props;
        !studentScore && stopFlag === 2 && dispatch(openShowGradeBox())
        this.setState({
            data
        })
    }
    showModal = () => {
        document.body.style.overflow = 'hidden'
        this.setState({
            visible: true,
        });
    }
    handleCancel = (e) => {
        document.body.style.overflow = 'auto'
        this.setState({
            visible: false,
        });
    }
    onClickToStudy = () => {
        const { courseId, id, studentCourseHomeworkInfo: { teacherCourseId } } = this.state.data;
        window.location.href = (`${jumpApi}?type=2&courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`)
    }
    toStudyOrCreate = ({ state }) => {
        const { history } = this.props;
        const { courseId, id, studentCourseHomeworkInfo: { teacherCourseId } } = this.state.data;
        if (state > 1) {
            history.push({
                pathname: `/personal-center/my-homework/${courseId}/${id}`,
                search: `?courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`
            })
        }
        else {
            window.location.href = (`${jumpApi}?type=2&courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`)
        }
    }
    closeGradeBox = () => {
        const { dispatch } = this.props;
        dispatch(closeShowGradeBox())
    }
    closeCartoonBox = () => {
        this.setState({ showCartoonBox: false })
    }
    onChangeRateValue = (value) => {
        this.setState({ rateValue: value })
    }
    onClickUploadGrade = (courseId, teacherCourseId) => {
        const { rateValue } = this.state;
        const { dispatch } = this.props;
        if (!rateValue) {
            message.info('我们的老师很辛苦，给个合适的评分呗~', 1)
            return
        }
        dispatch(postGradeForTeacher({ courseId, teacherCourseId, score: rateValue * 20 }))
    }
    render() {
        const { courseList, showGradeBox, courseDistribution } = this.props;
        const { data: { teacherCourseId, knowledges, title, id, studentCourseHomeworkInfo, coverUrl, videoUrl, lockFlag, number, startAt },
            visible, rateValue, showCartoon, showCartoonBox } = this.state;
        const { qrCodeUrl4Official = '', qrCodeUrl4Teacher = '' } = courseDistribution;
        return (
            <div className='course'>
                {courseList && (courseList.length !== 0 ? <div>
                    <Card style={{ borderRadius: '10px' }}>
                        <Scratch><img
                            style={{ width: '22px', height: '22px', marginRight: '9px' }}
                            src={`${process.env.PUBLIC_URL}/assets/icon-scratch-lesson.png`}
                            alt='' />{courseList[0].name}</Scratch>
                        <div className='parent'>
                            <div className='stable'>
                                <img style={{ width: '100%', height: '100%', borderRadius: '10px' }} src={coverUrl} alt='' />
                                <div className='stable-icon'>
                                    {lockFlag === 1 ? <img
                                        onClick={() => { message.info('您的课程还未到开放时间呢~') }}
                                        style={{ width: '100px', height: '100px', cursor: 'pointer' }}
                                        src={`${process.env.PUBLIC_URL}/assets/icon-lock.svg`} alt='' />
                                        :
                                        <Icon onClick={this.showModal} type="play-circle" />}
                                </div>
                            </div>
                            <div className='change'>
                                <Row>
                                    <div className='change-row1'>
                                        <div style={{ marginRight: '28px' }}>
                                            {`第${number}课时`}
                                        </div>
                                        {title}
                                    </div>
                                    <div className='change-row2'>课程知识点：</div>
                                    <div className='change-row3'>
                                        {
                                            knowledges && knowledges.split('<br/>').map((item, index) => {
                                                return <div key={index}>{item}</div>
                                            })}
                                    </div>
                                </Row>
                                <Row type='flex' justify='end' align='middle'>
                                    <Button
                                        disabled={lockFlag === 1}
                                        onClick={this.toStudyOrCreate.bind(this, studentCourseHomeworkInfo)}
                                        type='primary'
                                        style={{ minWidth: '136px', height: '38px', borderRadius: '38px', }}>
                                        {studentCourseHomeworkInfo && studentCourseHomeworkInfo.state > 1 ? '查看作业作品' : '开始学习'}
                                    </Button>
                                </Row>
                                {lockFlag === 1 && <div onMouseEnter={() => { this.setState({ showCartoon: true }) }}
                                    onMouseLeave={() => { this.setState({ showCartoon: false }) }}
                                    className='change-cartoon'>
                                    <img
                                        src={`${process.env.PUBLIC_URL}/assets/cartoon.gif`}
                                        alt='' />
                                    {
                                        showCartoon && <div className='user-cartoon'>
                                            <div className='user-cartoon-content' >
                                                <div style={{ paddingBottom: '8px' }}>
                                                    <div>亲爱的小朋友，本课时的开放</div>
                                                    时间：<span style={{ fontWeight: 'bold' }}>
                                                        {moment(startAt).format('YYYY年MM月DD日\xa0HH:mm')}
                                                    </span>
                                                    <div>很快你就可以学习编程创作啦！</div>
                                                </div>
                                                <Button
                                                    onClick={() => { this.setState({ showCartoonBox: true }) }}
                                                    style={{ background: '#fff', color: '#555555' }}
                                                    type='primary'>课前准备</Button >
                                            </div>
                                        </div>
                                    }
                                </div>}
                            </div>
                        </div>
                    </Card>
                    <Card style={{ borderRadius: '10px', marginTop: '30px' }}>
                        {
                            courseList.map(item => {
                                return <Row className='course-row' type='flex' justify='start' key={item.id}>{
                                    item.courseLessonInfoList.map(
                                        itema => {
                                            return <Col className='course-row-col'
                                                onClick={this.courseClick.bind(this, itema, item.studentScore, item.stopFlag)}
                                                span={6}
                                                key={itema.id}>
                                                <Userlistitem hasChoose={itema.id === id}><img src={itema.coverUrl}
                                                    style={{
                                                        width: '100%',
                                                        height: '196px',
                                                        borderTopLeftRadius: '10px',
                                                        borderTopRightRadius: '10px'
                                                    }} alt='' />
                                                    <div style={{
                                                        width: '100%',
                                                        height: '43px',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center'
                                                    }}>
                                                        <div style={{ marginRight: '15px' }}>
                                                            {`第${itema.number}课时`}
                                                        </div>
                                                        {itema.title}
                                                    </div>
                                                    <div className='course-row-col-header'>{`第${itema.number}课时`}</div>
                                                    {itema.lockFlag === 1 && <div className='course-row-col-icon'>
                                                        <img
                                                            style={{ width: '66px', height: '66px', marginBottom: '8px', cursor: 'pointer' }}
                                                            src={`${process.env.PUBLIC_URL}/assets/icon-lock.svg`} alt='' />
                                                        <div style={{ textAlign: 'center' }}>
                                                            {`待开放\xa0${moment(itema.startAt).format('YYYY年MM月DD日\xa0HH:mm')}`}
                                                        </div>
                                                    </div>}
                                                </Userlistitem>
                                            </Col>
                                        }
                                    )
                                }</Row>
                            })
                        }
                    </Card>
                </div> : <Row type='flex' justify='center' align='middle' style={{ minHeight: '80vh', background: '#fff', borderRadius: '10px' }}>
                        <div className='course-blank'>
                            <img style={{ width: '212px', height: '170px' }} src={`${process.env.PUBLIC_URL}/assets/nocourse.png`} alt='' />
                            <div className='course-blank-div'>你还没有参加任何课程哦~</div>
                        </div></Row>)}
                {visible && <div onClick={this.handleCancel} className='show-background'>
                    <div onClick={(e) => { e.stopPropagation() }} className='show-video-content'>
                        <Myvideo videoUrl={videoUrl}
                            onClickToStudy={this.onClickToStudy.bind(this)}
                            handleCancel={this.handleCancel.bind(this)} />
                    </div>
                </div>
                }
                <Modal
                    width={280}
                    visible={showGradeBox}
                    footer={null}
                    onCancel={this.closeGradeBox}
                    bodyStyle={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'column' }}
                >
                    <div className='grade-modal-title'>
                        <img
                            style={{ width: '20px', height: '20px' }}
                            src={`${process.env.PUBLIC_URL}/assets/icon-smile-grade.png`} alt='' />{`\xa0恭喜你学完全部课程！`}</div>
                    <div style={{ fontSize: '16px' }}>给辅导老师打个分吧！</div>
                    <div className='grade-modal-content'>我的评分：
                    <Rate allowClear={false} allowHalf onChange={this.onChangeRateValue} value={rateValue} /></div>
                    <Button className='grade-modal-button' type='primary' onClick={this.onClickUploadGrade.bind(this, id, teacherCourseId)}>上传评分</Button>
                </Modal>
                <Modal
                    width={820}
                    visible={showCartoonBox}
                    footer={null}
                    closable={false}
                    onCancel={this.closeCartoonBox}
                    bodyStyle={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'column' }}
                >
                    <div className='cartoon-box-title'>课前准备</div>
                    <div style={{ width: '100%', padding: '44px 94px' }}>
                        < Orderdistribution qrCodeUrl4Official={qrCodeUrl4Official} qrCodeUrl4Teacher={qrCodeUrl4Teacher} />
                    </div>
                </Modal>
            </div>
        )
    }
}
export default MyCourseList;