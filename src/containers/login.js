import React, { PureComponent } from 'react';
import {
    withRouter
} from "react-router-dom";
import './login.less';
import { Row, Input, Icon, Checkbox, Button } from 'antd';
import * as message from 'Message';
import { weixinLoginConfirm, weixinLoginForCode } from 'Redux/actions/loginactions.js';
import { connect } from 'react-redux';
@connect(({ login: { token, ssoInfo } }) => ({ token, ssoInfo }))
class Login extends PureComponent {
    state = {
        time: 60,
        disabled: false,
        phone: '',
        code: '',
        checked: true
    };
    componentDidMount() {
        this.timer = setInterval(() => {
            const { time } = this.state;
            if (time === 1) {
                this.setState({
                    time: 60,
                    disabled: false
                })
            } else {
                this.setState({ time: time - 1 })
            }
        }, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.timer)
    }
    onClickLogin = (ssoId, oauthId, avatarUrl, name) => {
        const { phone, code, checked } = this.state;
        const { dispatch } = this.props;
        if (!(/^1(3|4|5|7|8)\d{9}$/.test(phone))) {
            message.error('请绑定正确的手机号码', 1)
            return
        }
        else if (!code) {
            message.info('请输入验证码', 1)
            return
        }
        else if (!checked) {
            message.info('请勾选《用户协议》', 1)
            return
        }
        dispatch(weixinLoginConfirm({ ssoId, oauthId, verifyPhone: phone, verifyCode: code, avatarUrl, name }))
    }
    postMessage = (ssoId, oauthId) => {
        const { dispatch } = this.props;
        const { phone } = this.state
        if (!(/^1(3|4|5|7|8)\d{9}$/.test(phone))) {
            message.error('请绑定正确的手机号码', 1)
            return
        }
        dispatch(weixinLoginForCode({ ssoId, oauthId, verifyPhone: phone }))
        this.setState({ disabled: true, time: 60 })
    }
    onChangePhone = (e) => {
        this.setState({ phone: e.target.value })
    }
    onChangeCode = (e) => {
        this.setState({ code: e.target.value })
    }
    onChangeCheck = (e) => {
        this.setState({ checked: e.target.checked })
    }
    render() {
        let { time, disabled } = this.state;
        const { ssoInfo } = this.props;
        return (
            <div className='login-parent' >
                {ssoInfo ? <div className='login'>
                    <Row className='row1' type='flex' justify='start' align='middle'>
                        <img style={{ width: '116px' }} src={`${process.env.PUBLIC_URL}/assets/logo.png`} alt='' />
                    </Row>
                    <div className='login-div'>
                        <Row className='row2'>请绑定用于接收课程消息通知的手机号</Row>
                        <Row className='row3'>
                            <Input
                                onChange={this.onChangePhone}
                                style={{ height: '44px' }}
                                size='large'
                                allowClear
                                placeholder='请输入手机号码'
                                prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            /></Row>
                        <Row className='row4'>
                            <Input
                                onChange={this.onChangeCode}
                                placeholder='请输入验证码'
                                size='large'
                                style={{ height: '44px' }}
                            />
                            <Button
                                disabled={disabled}
                                onClick={this.postMessage.bind(this, ssoInfo.id, ssoInfo.oauthId)}
                                style={{ width: '100px', marginLeft: '24px', height: '44px' }}
                                type='primary' >{!disabled ? '获取验证码' : `${time}s`}</Button></Row>
                        <Row className='row5' type='flex' justify='start' align='middle' >
                            <Checkbox defaultChecked={true} onChange={this.onChangeCheck} />
                            <div style={{ marginLeft: '6px' }}>我已阅读并同意<a
                                rel='noreferrer noopener'
                                href='https://www.haimacode.com/test/HaimaStudent/student/agreement.html' target="_blank">《用户协议》</a></div>
                        </Row>
                        <Row className='row6'>
                            <Button
                                onClick={this.onClickLogin.bind(this, ssoInfo.id, ssoInfo.oauthId, ssoInfo.avatarUrl, ssoInfo.nickname)}
                                style={{ width: '306px', height: '44px', borderRadius: '44px', marginBottom: '50px', fontSize: '16px' }}
                                type='primary'
                            >登录
                    </Button></Row>
                    </div>
                </div>
                    : <iframe
                        className='login-iframe'
                        title='works-detail'
                        seamless='seamless'
                        frameBorder='0'
                        src={`https://open.weixin.qq.com/connect/qrconnect?appid=wx2b8b6b3771f9a10e&redirect_uri=https://www.haimacode.com/test/HaimaStudent/student/index.html?pathname=${window.location.pathname}&scope=snsapi_login#wechat_redirect`} >
                    </iframe>
                }
            </div>
        );
    }
}
export default withRouter(Login);
