import React, { Component } from 'react';
import {
  HashRouter as Router,
  Route,
  Redirect,
  Switch,
  NavLink
} from "react-router-dom";
import './app.less'
import { Layout, Row, Col, Icon, Avatar, Modal, Spin } from 'antd';
import styled from 'styled-components';
import { connect } from 'react-redux'
import { saveLoginToken, deleteLoginInfo, showLoginModal } from 'Redux/actions/loginactions.js';
import jumpApi from 'Config/jumpApi.js';
import Login from 'Containers/login.js';
const Paysuccess = React.lazy(() => import('Containers/payment/paySuccess.js'))
const Worksshow = React.lazy(() => import('Containers/worksShow.js'));
const Homepage = React.lazy(() => import('Containers/homepage.js'));
const Worksdtail = React.lazy(() => import('Containers/worksDetail.js'));
// const Mycourse = React.lazy(() => import('Containers/myCourse.js'));
const HomeworkDetail = React.lazy(() => import('Containers/homeworkDetail.js'));
const Publishworks = React.lazy(() => import('Containers/publishWorks.js'));
const Notification = React.lazy(() => import('Containers/notification.js'));
const Personalhomework = React.lazy(() => import('Containers/personalCenter/personalHomework.js'));
const Personalworks = React.lazy(() => import('Containers/personalCenter/personalWorks.js'));
const Personalorder = React.lazy(() => import('Containers/personalCenter/personalOrder.js'));
const Personalfeedback = React.lazy(() => import('Containers/personalCenter/personalFeedback.js'));
const Personalcollection = React.lazy(() => import('Containers/personalCenter/personalCollection.js'));
const Personalaccount = React.lazy(() => import('Containers/personalCenter/personalAccount.js'));
const Orderpayment = React.lazy(() => import('Containers/payment/orderPayment.js'));
const MyCourseList = React.lazy(() => import('Containers/myCourseList.js'));

const Userlink = styled.div`
color:#ffffff;
cursor: pointer;
transition: all 0.3s;
&:hover{
  transform: scale(1.1);
}
`

const { Header, Content } = Layout;
@connect(({ login: { token, userInfo, visible } }) => ({ token, userInfo, visible }))
class App extends Component {
  constructor(props) {
    super(props);
    this.token = window.localStorage.getItem('h_token')
    this.token && this.props.dispatch(saveLoginToken(this.token))
    this.state = {
      isAuthenticated: this.token ? true : false
    }
  }
  componentDidMount(){
    console.log('恭喜你喜提彩蛋！\n既然有天赋，那就好好学吧')
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (!prevState.isAuthenticated && nextProps.token) {
      return {
        ...prevState,
        isAuthenticated: true
      }
    }
    else if (prevState.isAuthenticated && !nextProps.token && !window.localStorage.getItem('h_token')) {
      return {
        ...prevState,
        isAuthenticated: false
      }
    }
    return null
  }
  onClickQuit = () => {
    const { dispatch } = this.props;
    dispatch(deleteLoginInfo())
    window.localStorage.removeItem('h_token')
    this.setState({ isAuthenticated: false })
  }
  onClickLogin = () => {
    const { dispatch } = this.props;
    dispatch(showLoginModal(true))
  }
  handleCancel = () => {
    const { dispatch } = this.props;
    dispatch(showLoginModal(false))
  }
  onClickCreate = () => {
    window.open(`${jumpApi}?type=3&productionId=0`)
  }
  render() {
    const { isAuthenticated } = this.state;
    const { userInfo, visible } = this.props;
    return (
      <Router >
        <React.Suspense fallback={<div className='user-lazy-div'><Spin size='large' tip='疯狂加载中...' /></div>}>
          <Layout className="layout"
            style={{
              minWidth: '1280px',
              background: `url(${process.env.PUBLIC_URL}/assets/main-background.png)  top`,
              backgroundSize: 'cover',
              backgroundAttachment:'fixed'
            }}>
            <Header className='header-home' >
              <Row className='header-home-nav' type='flex' justify='space-between' align='middle'>
                <Row type='flex' justify='start' align='middle' style={{ minWidth: '400px' }} >
                  <NavLink className='link-user' to='/'>
                    <img className="logo"
                      src={`${process.env.PUBLIC_URL}/assets/logo.png`} alt='' /></NavLink>
                  <NavLink activeClassName='link-user-choose' exact className='link-user-change' to='/' style={{ padding: '0 29px' }}>首页</NavLink>
                  <NavLink activeClassName='link-user-choose' className='link-user-change' to='/works-show'>作品展示</NavLink>
                  {isAuthenticated && <NavLink activeClassName='link-user-choose' className='link-user-change' to='/my-course'>我的课程</NavLink>}
                </Row>
                <Row type='flex' justify='end' align='middle' style={{ minWidth: '500px' }}>
                  <Userlink className='creation-user' onClick={this.onClickCreate}>我要创作</Userlink>
                  {!isAuthenticated ? <div onClick={this.onClickLogin} className='link-user-change' ><Icon type="user" />{`\xa0登录/注册`}</div> :
                    <Row type='flex' justify='end' align='middle'>
                      <NavLink activeClassName='link-user-choose' exact className='link-user-change' to='/notification'>
                        <div className='for-big' style={{ position: 'relative' }}>
                          <img
                            src={`${process.env.PUBLIC_URL}/assets/icon-bell.png`}
                            style={{ width: '23px', height: '23px' }} alt='' />
                          {userInfo.notifyCount ? <Badgeicon count={userInfo.notifyCount} overflowCount={9} /> : null}
                        </div>
                      </NavLink>
                      <NavLink activeClassName='link-user-choose' className='link-user-change' to='/personal-center'>
                        <Avatar className='for-big' size={30} icon='user' src={userInfo.avatarUrl} /></NavLink>
                      <div onClick={this.onClickQuit} className='link-user-change'><Icon type="poweroff" />{`\xa0退出`}</div>
                    </Row>}
                  <Col></Col>
                </Row>
              </Row>
            </Header>
            <Content className='content'>
              <Switch>
                <PrivateRoute isAuthenticated={isAuthenticated} exact path="/order-payment/:cid" component={Orderpayment} />
                <PrivateRoute isAuthenticated={isAuthenticated} exact path="/order-payment/:cid/:oid" component={Paysuccess} />
                {/* <PrivateRoute isAuthenticated={isAuthenticated} exact path="/order-payment" component={Orderpayment} /> */}
                <Route exact path="/" component={Homepage} />
                <Route exact path="/works-show" component={Worksshow} />
                <PrivateRoute isAuthenticated={isAuthenticated} exact path="/my-course" component={MyCourseList} />
                {/* <PrivateRoute isAuthenticated={isAuthenticated} path="/my-course/:mcid" component={Mycourse} /> */}
                <PrivateRoute isAuthenticated={isAuthenticated} path="/notification" component={Notification} />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/publish-works" component={Publishworks} />
                <Route path="/works-show/:id" render={(props) => { return <Worksdtail {...props} key={props.match.params.id} /> }} />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/personal-center/my-homework/:csid/:hid" component={HomeworkDetail} />
                {/* <PrivateRoute isAuthenticated={isAuthenticated} exact path="/personal-center/my-homework/:csid" component={Personalhomework} /> */}
                <Redirect exact from="/personal-center" to="/personal-center/my-works" />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/personal-center/my-works" component={Personalworks} />
                <PrivateRoute isAuthenticated={isAuthenticated} exact path="/personal-center/my-homework" component={Personalhomework} />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/personal-center/my-collection" component={Personalcollection} />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/personal-center/order-management" component={Personalorder} />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/personal-center/account-settings" component={Personalaccount} />
                <PrivateRoute isAuthenticated={isAuthenticated} path="/personal-center/question-feedback" component={Personalfeedback} />
                <Redirect to='/' />
              </Switch>
              <Modal
                bodyStyle={{ padding: 0 }}
                visible={visible}
                onCancel={this.handleCancel}
                footer={null}
                closable={false}
                destroyOnClose={true}
              >
                <Login />
              </Modal>
            </Content>
          </Layout>
        </React.Suspense>
      </Router>
    );
  }
}

export default App;


const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location }
              }}
            />
          )
      }
    />
  );
}
const Badgeicon = React.memo(
  ({ count = 0, overflowCount = 0 }) => {
    return <div className='badge-icon'>{
      count > overflowCount ? `···` : count
    }</div>
  }
)
