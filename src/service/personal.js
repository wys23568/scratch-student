import Axios from './axios.js'
export const getMyWorksList = async (data) => {
    return Axios.post('/haimawang/account/student/production/list', data)
}
export const deleteMyWorks = async (data) => {
    return Axios.post('/haimawang/account/student/production/delete', data)
}
export const getMyHomeworksList = async (data) => {
    return Axios.post('/haimawang/account/student/course/homework/list', data)
}
export const getMyCollectionList = async (data) => {
    return Axios.post('/haimawang/account/student/production/collect/list', data)
}
export const getMyLikeList = async (data) => {
    return Axios.post('/haimawang/account/student/production/like/list', data)
}
export const getMyOrderList = async (data) => {
    return Axios.post('/haimawang/account/student/course/order/list', data)
}
export const doApplyRefund = async (data) => {
    return Axios.post('/haimawang/account/student/course/order/applyRefund', data)
}
export const getPersonalInfo = async (data) => {
    return Axios.post('/haimawang/account/student/info', data)
}
export const updateUserInfo = async (data) => {
    return Axios.post('/haimawang/account/student/update', data)
}
export const questionCommit = async (data) => {
    return Axios.post('/haimawang/account/student/comment/commit', data)
}
export const phoneUnbinding = async (data) => {
    return Axios.post('/service/account/common/phone/unbinding/request', data)
}
export const phoneUnbindingComfirm = async (data) => {
    return Axios.post('/service/account/common/phone/unbinding/confirm', data)
}
export const phoneBinding = async (data) => {
    return Axios.post('/service/account/common/phone/binding/request', data)
}
export const phoneBindingComfirm = async (data) => {
    return Axios.post('/service/account/common/phone/binding/confirm', data)
}
export const getCourseHomeworkList = async (data) => {
    return Axios.post('/haimawang/account/student/course/list', data)
}