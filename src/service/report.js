import Axios from './axios.js'
export const getReportList = async () => {
    return Axios.get('/haimawang/config/reportConfig')
}
export const postReport=async(data)=>{
    return Axios.post('/haimawang/account/student/production/report',data)
}