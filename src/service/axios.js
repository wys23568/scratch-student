import axios from 'axios';
import Qs from 'qs';
import requestApi from 'Config/requestApi.js';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import confirmArg from 'Config/confirmArg.js';
const Axios = axios.create({
    baseURL: requestApi,
    timeout: 10000,
    headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
        // 对 data 进行任意转换处理
        return Qs.stringify({ ...data, ...confirmArg });
    }],
    transformResponse: [function (data) {
        // 对 data 进行任意转换处理
        return JSON.parse(data);
    }]
});
// 添加请求拦截器
Axios.interceptors.request.use(function (config) {
    NProgress.start()
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
Axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    NProgress.done()
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});
export default Axios;