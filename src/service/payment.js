import Axios from './axios.js'
export const createOrder = async (data) => {
    return Axios.post('/haimawang/account/student/course/order/create', data)
}
export const getOrderDetail = async (data) => {
    return Axios.post('/haimawang/course/detail', data)
}
export const confirmOrderStatus = async (data) => {
    return Axios.post('/haimawang/account/student/course/order/detail', data)
}
export const getOrderDistribution = async (data) => {
    return Axios.post('/haimawang/account/student/course/order/finish', data)
}
export const continuePaying = async (data) => {
    return Axios.post('/haimawang/account/student/course/order/pay', data)
}