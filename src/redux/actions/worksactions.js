import * as worksService from 'Service/worksShow.js';
import * as message from 'Message';
export const getWorkList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.getWorksList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        const requestList = new Map([['1', { type: 'saveSelectedWorks', name: 'selectedWorks' }],
        ['2', { type: 'saveRecommendWorks', name: 'recommendWorks' }], [
            '3', { type: 'saveNewWorks', name: 'newWorks' }
        ]])
        dispatch({ type: requestList.get(data.type).type, [`${requestList.get(data.type).name}`]: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getWorkDetail = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.getWorksDetail({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveWorksDetail', worksDetail: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const doLike = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.doLike({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'doLike', likeFlag: 2 })
        dispatch(getWorkDetail(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const cancelLike = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.cancelLike({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'cancelLike', likeFlag: 1 })
        dispatch(getWorkDetail(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const doCollected = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.doCollected({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'doCollected', collectFlag: 2 })
        dispatch(getWorkDetail(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const cancelCollected = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.cancelCollected({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'cancelCollected', collectFlag: 1 })
        dispatch(getWorkDetail(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getProductionTag = (data) => (dispatch, getState) => {
    worksService.getProductionTag().then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveProductionTag', productionTag: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getProductionCover = (data) => (dispatch, getState) => {
    worksService.getProductionCover().then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveProductionCover', productionCover: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getCommentList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.getCommentList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveCommentList', commentList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getReplyList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.getReplyList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveReplyList', replyContent: { content: response.data.data.content, id: data.commentId } })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const postComment = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.postComment({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        message.success('评论成功', 1)
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const postReply = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.postReply({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        message.success('回复成功', 1)
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const doCommentLike = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.doCommentLike({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'doCommentLike', like: { likeFlag: 2, id: data.commentId } })
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const cancelCommentLike = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.cancelCommentLike({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'cancelCommentLike', like: { likeFlag: 1, id: data.commentId } })
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const doReplyLike = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.doReplyLike({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'doReplyLike', replyLike: { likeFlag: 2, commentId: data.commentId, replyId: data.replyId } })
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const cancelReplyLike = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.cancelReplyLike({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'cancelReplyLike', replyLike: { likeFlag: 1, commentId: data.commentId, replyId: data.replyId } })
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const deleteComment = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.deleteComment({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const deleteReply = (data) => (dispatch, getState) => {
    const { token } = getState().login
    worksService.deleteReply({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch(getCommentList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}