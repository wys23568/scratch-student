import * as personalService from 'Service/personal.js';
import * as message from 'Message';
export const getMyWorksList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.getMyWorksList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveMyWorksList', myWorksList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const deleteMyWorks = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.deleteMyWorks({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch(getMyWorksList(data))
        message.success('删除成功', 1)
    })
        .catch(function (error) {
            console.log(error);
        });
}

export const getMyHomeworksList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.getMyHomeworksList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveMyHomeworksList', myHomeworksList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getMyCollectionList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.getMyCollectionList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveMyCollectionList', myCollectionList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getMyLikeList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.getMyLikeList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveMyLikeList', myLikeList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getMyOrderList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.getMyOrderList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveMyOrderList', myOrderList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const doApplyRefund = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.doApplyRefund({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch(getMyOrderList(data))
        message.success('申请成功', 1)
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const updateUserInfo = (data) => (dispatch, getState) => {
    const { token } = getState().login
    personalService.updateUserInfo({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveUserInfo', userInfo: response.data.data })
        dispatch({ type: 'savePersonalInfo', personalInfo: response.data.data })
        message.success('保存成功', 1)
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getPersonalInfo = (data = {}) => (dispatch, getState) => {
    const { token } = getState().login;
    personalService.getPersonalInfo({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'savePersonalInfo', personalInfo: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const updateAvatarUrl = (avatarUrl) => (dispatch, getState) => {
    dispatch({ type: 'updateAvatarUrl', avatarUrl })
}
export const updateGender = (gender) => (dispatch, getState) => {
    dispatch({ type: 'updateGender', gender })
}
export const updateName = (name) => (dispatch, getState) => {
    dispatch({ type: 'updateName', name })
}
export const phoneUnbinding = (data = {}) => (dispatch, getState) => {
    const { token } = getState().login;
    personalService.phoneUnbinding({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const phoneUnbindingComfirm = (data = {}) => (dispatch, getState) => {
    const { token } = getState().login;
    personalService.phoneUnbindingComfirm({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'phoneUnbindingComfirm', newOrOldType: 1 })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const phoneBinding = (data = {}) => (dispatch, getState) => {
    const { token } = getState().login;
    personalService.phoneBinding({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const phoneBindingComfirm = (data = {}) => (dispatch, getState) => {
    const { token } = getState().login;
    personalService.phoneUnbindingComfirm({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        message.success('手机换绑成功');
        dispatch({ type: 'phoneUnbindingComfirm', newOrOldType: 0 })
    })
        .catch(function (error) {
            console.log(error);
        });
}
//新增作业列表页后需更改
export const getCourseHomeworkList = (data = {}) => (dispatch, getState) => {
    const { token } = getState().login;
    personalService.getCourseHomeworkList({ pageNumber: 0, pageSize: 20, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch(getMyHomeworksList({ ...data, courseId: response.data.data.length > 0 && response.data.data[0].id }))
        dispatch({ type: 'saveCourseHomeworkList', saveCourseHomeworkList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}