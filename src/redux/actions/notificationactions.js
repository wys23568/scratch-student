import * as notificationService from 'Service/notification.js';
import { getUserInfo } from './loginactions.js';
import * as message from 'Message';
export const getNotificationList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    notificationService.getNotificationList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveNotificationList', notificationList: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const hasRead = (data) => (dispatch, getState) => {
    const { token } = getState().login
    notificationService.hasRead({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch(getNotificationList(data))
        dispatch(getUserInfo(token))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const readyToDelete = (data) => (dispatch, getState) => {
    const { token } = getState().login
    notificationService.readyToDelete({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch(getNotificationList(data))
    })
        .catch(function (error) {
            console.log(error);
        });
}