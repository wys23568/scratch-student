import * as homeworkService from 'Service/homework.js';
import * as message from 'Message';
export const getHomeworkDetail = (data) => (dispatch, getState) => {
    const { token } = getState().login
    homeworkService.getHomeworkDetail({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveHomeworkDetail', homeworkDetail: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}