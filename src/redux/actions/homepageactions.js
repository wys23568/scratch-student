import * as homepageService from 'Service/homepage.js';
import * as message from 'Message';
export const getHomepageInfo = (data) => (dispatch, getState) => {
    const { token } = getState().login;
    homepageService.getHomepageInfo({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveHomepageInfo', homepageInfo: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}