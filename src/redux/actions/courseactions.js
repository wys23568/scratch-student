import * as courseService from 'Service/course.js';
import * as message from 'Message';
export const getCourseList = (data) => (dispatch, getState) => {
    const { token } = getState().login
    courseService.getCourseList({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveCourseList', courseList: response.data.data });
        //暂用
        response.data.data[0] && dispatch(getCourseDistribution({ courseId: response.data.data[0].id}))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getCourseDetail = (data) => (dispatch, getState) => {
    const { token } = getState().login
    courseService.getCourseDetail({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveCourseDetail', courseDetail: response.data.data });
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const postGradeForTeacher = (data) => (dispatch, getState) => {
    const { token } = getState().login
    courseService.postGradeForTeacher({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        message.success('上传成功', 1)
        dispatch({ type: 'shouldShowGradeBox', showGradeBox: false })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const closeShowGradeBox = (data) => (dispatch, getState) => {
    dispatch({ type: 'shouldShowGradeBox', showGradeBox: false })
}
export const openShowGradeBox = (data) => (dispatch, getState) => {
    dispatch({ type: 'shouldShowGradeBox', showGradeBox: true })
}
export const getCourseDistribution = (data) => (dispatch, getState) => {
    const { token } = getState().login
    courseService.getCourseDistribution({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveCourseDistribution', courseDistribution: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}