const initialState = {
    courseList: null,
    showGradeBox: false,
    courseDetail: null,
    courseDistribution: {}
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveCourseList':
            return {
                ...state,
                courseList: action.courseList
            }
        case 'saveCourseDetail':
            return {
                ...state,
                courseDetail: action.courseDetail
            }
        case 'shouldShowGradeBox':
            return {
                ...state,
                showGradeBox: action.showGradeBox
            }
        case 'saveCourseDistribution':
            return {
                ...state,
                courseDistribution: action.courseDistribution
            }
        default:
            return state
    }
}