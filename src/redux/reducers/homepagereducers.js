const initialState = {
    homepageInfo:{}
}
export default (state = initialState, action) => {
    switch (action.type) {
       case 'saveHomepageInfo':
       return{
           ...state,
           homepageInfo:action.homepageInfo
       }
        default:
            return state
    }
}