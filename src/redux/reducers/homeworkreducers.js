const initialState = {
    homeworkDetail:{}
}
export default (state = initialState, action) => {
    switch (action.type) {
       case 'saveHomeworkDetail':
       return{
           ...state,
           homeworkDetail:action.homeworkDetail
       }
        default:
            return state
    }
}