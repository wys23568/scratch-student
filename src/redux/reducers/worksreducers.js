const initialState = {
    selectedWorks: {},
    recommendWorks: {},
    newWorks: {},
    worksDetail: {},
    productionTag: [],
    productionCover: [],
    commentList: {},
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveSelectedWorks':
            return {
                ...state,
                selectedWorks: action.selectedWorks
            }
        case 'saveRecommendWorks':
            return {
                ...state,
                recommendWorks: action.recommendWorks
            }
        case 'saveNewWorks':
            return {
                ...state,
                newWorks: action.newWorks
            }
        case 'saveWorksDetail':
            return {
                ...state,
                worksDetail: action.worksDetail
            }
        case 'doLike':
            return {
                ...state, worksDetail: {
                    ...state.worksDetail,
                    likeflag: action.likeflag,
                    likeCount: state.worksDetail.likeCount + 1
                }
            }
        case 'cancelLike':
            return {
                ...state, worksDetail: {
                    ...state.worksDetail,
                    likeflag: action.likeflag,
                    likeCount: state.worksDetail.likeCount - 1
                }
            }
        case 'doCollected':
            return {
                ...state, worksDetail: {
                    ...state.worksDetail,
                    collectFlag: action.collectFlag,
                }
            }
        case 'cancelCollected':
            return {
                ...state, worksDetail: {
                    ...state.worksDetail,
                    collectFlag: action.collectFlag,
                }
            }
        case 'saveProductionTag':
            return {
                ...state,
                productionTag: action.productionTag
            }
        case 'saveProductionCover':
            return {
                ...state,
                productionCover: action.productionCover
            }
        case 'saveCommentList':
            return {
                ...state,
                commentList: action.commentList
            }
        case 'saveReplyList':
            return {
                ...state,
                commentList: {
                    ...state.commentList,
                    content: state.commentList.content.map(item => {
                        if (action.replyContent.id === item.id) {
                            return {
                                ...item,
                                productionCommentReplyInfoList: [...item.productionCommentReplyInfoList, ...action.replyContent.content]
                            }
                        }
                        else {
                            return {
                                ...item
                            }
                        }
                    })
                }
            }
        case 'doCommentLike':
            return {
                ...state,
                commentList: {
                    ...state.commentList,
                    content: state.commentList.content.map(item => {
                        if (action.like.id === item.id) {
                            return {
                                ...item,
                                likeFlag: action.like.likeFlag,
                                likeCount: item.likeCount + 1
                            }
                        }
                        else {
                            return {
                                ...item
                            }
                        }
                    })
                }
            }
        case 'cancelCommentLike':
            return {
                ...state,
                commentList: {
                    ...state.commentList,
                    content: state.commentList.content.map(item => {
                        if (action.like.id === item.id) {
                            return {
                                ...item,
                                likeFlag: action.like.likeFlag,
                                likeCount: item.likeCount - 1
                            }
                        }
                        else {
                            return {
                                ...item
                            }
                        }
                    })
                }
            }
        case 'doReplyLike':
            return {
                ...state,
                commentList: {
                    ...state.commentList,
                    content: state.commentList.content.map(item => {
                        if (action.replyLike.commentId === item.id) {
                            return {
                                ...item,
                                productionCommentReplyInfoList: item.productionCommentReplyInfoList.map(itema => {
                                    if (action.replyLike.replyId === itema.id) {
                                        return {
                                            ...itema,
                                            likeFlag: action.replyLike.likeFlag,
                                            likeCount: itema.likeCount + 1
                                        }
                                    }
                                    else {
                                        return {
                                            ...itema
                                        }
                                    }
                                })
                            }
                        }
                        else {
                            return {
                                ...item
                            }
                        }
                    })
                }
            }
        case 'cancelReplyLike':
            return {
                ...state,
                commentList: {
                    ...state.commentList,
                    content: state.commentList.content.map(item => {
                        if (action.replyLike.commentId === item.id) {
                            return {
                                ...item,
                                productionCommentReplyInfoList: item.productionCommentReplyInfoList.map(itema => {
                                    if (action.replyLike.replyId === itema.id) {
                                        return {
                                            ...itema,
                                            likeFlag: action.replyLike.likeFlag,
                                            likeCount: itema.likeCount - 1
                                        }
                                    }
                                    else {
                                        return {
                                            ...itema
                                        }
                                    }
                                })
                            }
                        }
                        else {
                            return {
                                ...item
                            }
                        }
                    })
                }
            }
        default:
            return state
    }
}