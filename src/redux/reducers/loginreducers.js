const initialState = {
    token: '',
    userInfo: {},
    visible: false,
    ssoInfo:null
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveLoginToken':
            return {
                ...state, token: action.token
            }
        case 'saveUserInfo':
            return {
                ...state,
                userInfo: action.userInfo
            }
        case 'deleteLoginInfo':
            return {
                ...state,
                userInfo: {},
                token: '',
                ssoInfo:null
            }
        case 'showLoginModal':
            return {
                ...state,
                visible: action.visible
            }
            case 'saveSsoInfo':
                return{
                    ...state,
                    ssoInfo:action.ssoInfo
                }
        default:
            return state
    }
}

