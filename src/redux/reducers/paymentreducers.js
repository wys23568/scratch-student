const initialState = {
    payVisible: false,
    orderInfo: {},
    orderDetail: {},
    orderDistribution:{}
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveOrderInfo':
            return {
                ...state,
                orderInfo: action.orderInfo
            }
        case 'showOrClosePayModal':
            return {
                ...state,
                payVisible: action.payVisible
            }
        case 'saveOrderDetail':
            return {
                ...state,
                orderDetail: action.orderDetail
            }
        case 'saveOrderDistribution':
            return {
                ...state,
                orderDistribution: action.orderDistribution
            }
        default:
            return state
    }
}