import { combineReducers } from 'redux';
import login from './loginreducers';
import works from './worksreducers';
import report from './reportreducers';
import course from './coursereducers';
import homework from './homeworkreducers';
import notification from './notificationreducers';
import personal from './personalreducers';
import publish from './publishreducers';
import homepage from './homepagereducers';
import payment from './paymentreducers';

export default combineReducers({
    login,
    works,
    report,
    course,
    homework,
    notification,
    personal,
    publish,
    homepage,
    payment
})