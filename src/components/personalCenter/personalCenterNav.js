import React from 'react';
import './personalCenterNav.less';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
const Usernav = styled.div`
   cursor: pointer;
   width:186px;
   height: 52px;
   padding: 0 40px;
   display:flex;
   justify-content:flex-start;
   align-items:center;
   color:${props => props.checked ? '#54C3D4' : '#999999'};
   border-radius:52px;
   font-size:15px;
   background:${props => props.checked ? '#EEF8FB' : '#fff'};
   transition: all 0.3s;
   &:hover{
       color:#54C3D4;
   }
`
const Personalcenternav = React.memo(
    withRouter(
        ({ userInfo = {}, location, history }) => {
            const navList = [{ path: '/personal-center/my-works', name: '我的作品', src: `${process.env.PUBLIC_URL}/assets/icon-myworks-` },
            { path: '/personal-center/my-homework', name: '我的作业', src: `${process.env.PUBLIC_URL}/assets/icon-myhomework-` },
            { path: '/personal-center/my-collection', name: '点赞与收藏', src: `${process.env.PUBLIC_URL}/assets/icon-mylike-` },
            { path: '/personal-center/order-management', name: '订单管理', src: `${process.env.PUBLIC_URL}/assets/icon-myshops-` },
            { path: '/personal-center/account-settings', name: '账号设置', src: `${process.env.PUBLIC_URL}/assets/icon-mycounts-` },
            { path: '/personal-center/question-feedback', name: '问题反馈', src: `${process.env.PUBLIC_URL}/assets/icon-myfeedback-` }]
            const onClick = (path) => {
                history.push(path)
            }
            return (
                <div className='personal-nav'>
                    <img style={{ width: '85px', height: '85px', borderRadius: '85px' }} src={userInfo.avatarUrl} alt='' />
                    <div className='personal-nav-name'>{userInfo.name}</div>
                    {
                        navList.map(item => {
                            let checked = item.path === location.pathname
                            return <Usernav
                                onClick={() => onClick(item.path)}
                                checked={checked}
                                key={item.path}>
                                <img style={{ width: '18px', height: '18px', marginRight: '12px'}}
                                    src={`${item.src}${checked ? 'light' : 'dark'}.png`}
                                    alt='' />
                                {item.name}
                            </Usernav>
                        })
                    }
                </div>
            )
        }
    )
)
export default Personalcenternav;