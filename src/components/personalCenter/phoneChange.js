import React, { useState, useEffect, useRef } from 'react';
import './phoneChange.less';
import { Input, Button } from 'antd';
import * as message from 'Message';
const Phonechange = React.memo(
    ({ placeholder, oldComfirm, newComfirm, getCode, type, oldPhone }) => {
        const [disabled, setDisabled] = useState(false);
        const [time, setTime] = useState(60);
        const [open, setOpen] = useState(false);
        const [code, setCode] = useState('')
        const [number, setNumber] = useState('')
        useEffect(() => {
            setNumber('')
            setCode('')
        }, [type])
        useInterval(() => {
            if (open) {
                if (time === 1) {
                    setDisabled(false)
                    setTime(60)
                    setOpen(false)
                }
                else {
                    setDisabled(true)
                    setTime(time - 1)
                }
            }
        }, 1000);
        const postMessage = () => {
            if (!(/^1(3|4|5|7|8)\d{9}$/.test(number))) {
                message.error('请输入正确的手机号码~')
                return
            }
            else if (!type && (number !== oldPhone)) {
                message.error('输入号码与原绑定手机号码不一致~')
                return
            }
            else if (type && (number === oldPhone)) {
                message.error('输入号码与原绑定手机号码一致~')
                return
            }
            getCode(number, type)
            setOpen(true)
        }
        const onClickComfirm = () => {
            if (!(/^1(3|4|5|7|8)\d{9}$/.test(number))) {
                message.error('请输入正确的手机号码~')
                return
            }
            else if (!type && (number !== oldPhone)) {
                message.error('输入号码与原绑定手机号码不一致~')
                return
            }
            else if (type && (number === oldPhone)) {
                message.error('输入号码与原绑定手机号码一致~')
                return
            }
            else if (!code) {
                message.info('请输入验证码~')
                return
            }
            type ? newComfirm(number, code) : oldComfirm(number, code)

        }
        const onChangeCode = (e) => {
            setCode(e.target.value)
        }
        const onChangeNumber = (e) => {
            setNumber(e.target.value)
        }
        return (
            <div className='phone'>
                <div className='phone-title'>手机换绑</div>
                <div className='phone-input'><Input
                    allowClear
                    maxLength={11}
                    size='large'
                    placeholder={placeholder}
                    onChange={onChangeNumber}
                /></div>
                <div className='phone-message'>
                    <Input
                        maxLength={6}
                        placeholder='请输入验证码'
                        size='large'
                        onChange={onChangeCode}
                    />
                    <Button
                        disabled={disabled}
                        onClick={postMessage}
                        style={{ width: '100px', marginLeft: '14px', height: '40px' }}
                        type='primary' >
                        {!disabled ? '获取验证码' : `${time}s`}
                    </Button>
                </div>
                <div className='phone-button-parent'><Button
                    onClick={onClickComfirm}
                    className='phone-button'
                    size='large'
                    type='primary' >确定</Button></div>
            </div>
        )
    }
)
export default Phonechange;
const useInterval = (callback, delay) => {
    const savedCallback = useRef();
    useEffect(() => {
        savedCallback.current = callback;
    });
    useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}