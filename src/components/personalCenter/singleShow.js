import React from 'react';
import { withRouter } from 'react-router-dom';
import './singleShow.less';
import moment from 'moment';
import { Icon, Row, Col, Button } from 'antd';
const Singleshow = React.memo(
    withRouter(
        ({ data = {}, onDelete, onPublish, onEdit, renderType, toChangePage, total = 0 }) => {
            const { studentCourseHomeworkInfo = {} } = data;
            const operationList = [{ type: 1, src: 'icon-pub.png', name: '发布' },
            { type: 2, src: 'icon-edit.png', name: '编辑' },
            { type: 3, src: 'icon-delete.png', name: '删除' }];
            const operationListRender = data.state === 2 ? operationList.slice(1) : operationList;
            const onClickOperation = (type) => {
                const operationMap = new Map([[1, () => { onPublish(data) }], [2, () => { onEdit(data) }], [3, () => { onDelete(data, total) }]])
                let actions = operationMap.get(type)
                actions.call(this)
            }
            const onClickChangePage = () => {
                toChangePage(data)
            }
            return (
                <div className='single-show'>
                    <img onClick={onClickChangePage} className='single-show-img' src={data.coverUrl} alt='' />
                    {(renderType === 2 && studentCourseHomeworkInfo.state === 1 && data.lockFlag === 2) ?
                        <div className='single-show-tostudy'>
                            <Button
                                onClick={onClickChangePage}
                                type='primary'
                                className='single-show-tostudy-button'>去学习</Button>
                        </div>
                        :
                        <div className='single-show-bottom'>
                            <div className='single-show-name' style={{ color: (renderType === 2 && data.lockFlag === 1) ? '#999999' : '#444444' }}>
                                {
                                    renderType === 2 ? `第${data.number}课时\xa0\xa0${data.title}` : data.name
                                }
                            </div>
                            {renderType !== 3 && <div className='single-show-time'>
                                {
                                    (renderType === 2 && data.lockFlag === 1) ?
                                        `待开放课程：\xa0${moment(data.startAt).format("YYYY.MM.DD")}`
                                        :
                                        `更新时间：\xa0${moment(renderType === 1 ? data.updatedAt :
                                            studentCourseHomeworkInfo.updatedAt).format("YYYY.MM.DD\xa0HH:mm:ss")}`
                                }
                            </div>}
                            {
                                ((renderType === 1) || (renderType === 3)) && <div className='single-show-like'>
                                    <Icon style={{ marginRight: '6px', fontSize: '17px' }} type="eye" theme="filled" />{data.lookCount}
                                    <Icon style={{ margin: '0 6px 0 18px', fontSize: '17px' }} type="like" theme="filled" />{data.likeCount}
                                </div>
                            }
                            {
                                renderType === 1 && <Row
                                    type='flex'
                                    justify='start'
                                    align='middle'
                                    className='single-show-operation'>
                                    {
                                        operationListRender.map(item => {
                                            return <Col
                                                style={{ borderRight: (item.type === 1 || item.type === 2) && '1px solid #F0F0F0' }}
                                                span={data.state === 2 ? 12 : 8}
                                                key={item.name}
                                                className='single-show-operation-item'>
                                                <div
                                                    onClick={() => onClickOperation(item.type)}
                                                    className='single-show-operation-div'>
                                                    <img
                                                        style={{ width: '14px', height: '14px', marginRight: '6px' }}
                                                        src={`${process.env.PUBLIC_URL}/assets/${item.src}`} alt='' />
                                                    {item.name}
                                                </div>
                                            </Col>
                                        })
                                    }
                                </Row>
                            }
                            {
                                renderType === 3 && <div className='single-show-author'>{`作者：\xa0${data.authorName}`}</div>
                            }
                        </div>}
                    {
                        (renderType === 2 && studentCourseHomeworkInfo.state === 2 && data.lockFlag === 2) &&
                        <div className='single-show-position'><Icon type="edit" />草稿</div>
                    }
                    {
                        (renderType === 2 && data.lockFlag === 1) && <div onClick={onClickChangePage}
                            className='single-show-lock'>
                            <img
                                style={{ width: '66px', height: '66px', cursor: 'pointer' }}
                                src={`${process.env.PUBLIC_URL}/assets/icon-lock.svg`} alt='' />
                        </div>
                    }
                </div>
            )
        }
    )
)
export default Singleshow;