import React, { useState } from 'react';
import { Row, Col, Icon, Modal, Button } from 'antd';
import { withRouter } from 'react-router-dom';
import './orderItem.less';
import moment from 'moment';
const Orderitem = React.memo(
    withRouter(
        ({ history, toApplyRefund, data: { orderNo, payAt, courseInfo = {}, orderMoney, state, id } }) => {
            const [visible, setVisible] = useState(false)
            const { name, issue, id: cid } = courseInfo;
            const orderState = new Map([[1, '待付款'], [2, '已取消'], [3, '已付款'],
            [4, <div style={{ color: '#FF8548' }}>{`退款处理中\xa0`}<Icon type="clock-circle" /></div>],
            [5, <div style={{ color: '#FF8548' }}>{`退款处理中\xa0`}<Icon type="clock-circle" /></div>],
            [6, <div style={{ color: '#999999' }}>退款成功</div>], [7, '已关闭']])
            const onApplyRefund = () => {
                toApplyRefund(id)
                setVisible(false)
            }
            const toDoSomeOperation = (state) => {
                if (state === 1) {
                    history.push({
                        pathname: `/order-payment/${cid}`,
                        search: `?orderNo=${orderNo}`
                    })
                    return
                }
                setVisible(true)
            }
            const handleCancel = e => {
                setVisible(false)
            }
            return (
                <div className='order-item'>
                    <div className='order-item-div'>
                        <div style={{ paddingRight: '34px' }}>{`订单号：${orderNo}`}</div>
                        <div>{`下单时间：\xa0${payAt ? moment(payAt).format("YYYY-MM-DD\xa0HH:mm:ss") : '--'}`}</div>
                    </div>
                    <Row
                        type='flex'
                        justify='start'
                        align='middle'
                        className='order-item-row'
                    >
                        <Col span={8}>{name}</Col>
                        <Col span={4}>{issue}</Col>
                        <Col span={4}>{`${orderMoney}元`}</Col>
                        <Col span={4}>{orderState.get(state)}</Col>
                        <Col span={4}>{
                            state === 1 || (state === 3 && (courseInfo.courseType !== 1) && (courseInfo.state < 5)) ? <div
                                onClick={toDoSomeOperation.bind(this, state)}
                                style={{ color: '#54C3D4', cursor: 'pointer' }}
                            >{state === 1 ? '继续支付' : '申请退款'}</div>
                                : '无'
                        }</Col>
                    </Row>
                    <Modal
                        visible={visible}
                        width={400}
                        onCancel={handleCancel}
                        footer={null}
                        closable={false}
                    >
                        <div className='modal-style'>
                            <div className='modal-style-title' >退款申请</div>
                            <div style={{ fontSize: '15px', color: '#999999', padding: '5px 0 10px 0' }}>您确定要申请该订单的退款吗？</div>
                            <div style={{ fontSize: '15px', color: '#444444' }}>{`课程名称：\xa0${name}`}</div>
                            <div style={{ fontSize: '15px', color: '#54C3D4' }}>{`期次：\xa0${issue}`}</div>
                            <div style={{ fontSize: '15px', color: '#999999', paddingTop: '10px' }}>我们将在3个工作日内完成操作</div>
                            <div style={{ fontSize: '15px', color: '#999999' }}>退款将返还至您的支付账户，请注意查收。</div>
                            <div className='modal-style-comfirm'>
                                <Button
                                    onClick={handleCancel}
                                    className='modal-style-comfirm-button'>取消</Button>
                                <Button
                                    onClick={onApplyRefund}
                                    className='modal-style-comfirm-button' type='primary'>确认</Button>
                            </div>
                        </div>
                    </Modal>
                </div>
            )
        }
    )
)
export default Orderitem;