import React from 'react';
import { message } from 'antd';
import './message.less';
export const success = (content, duration = 1) => message.open({
    content,
    duration,
    icon: <img src={`${process.env.PUBLIC_URL}/assets/icon-success.png`} className='message-icon' alt='' />
})
export const info = (content, duration = 1) => message.open({
    content,
    duration,
    icon: <img src={`${process.env.PUBLIC_URL}/assets/icon-info.png`} className='message-icon' alt='' />
})
export const error = (content, duration = 1) => message.open({
    content,
    duration,
    icon: <img src={`${process.env.PUBLIC_URL}/assets/icon-error.png`} className='message-icon' alt='' />
})