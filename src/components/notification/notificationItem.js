import React, { useState } from 'react';
import './notificationItem.less';
import {
    withRouter
} from "react-router-dom";
import moment from 'moment';
import styled from 'styled-components';
const Userspot = styled.div`
width:9px;
height:9px;
background:#F86E6E;
border-radius:9px;
margin-right:6px;
`
const Notificationitem = React.memo(
    withRouter(
        ({ data = {}, toWorksDetail, deleteNotification, total }) => {
            const [show, setShow] = useState(false)
            const onMouseEnter = () => {
                setShow(true)
            }
            const onMouseLeave = () => {
                setShow(false)
            }
            const onClickProps = () => {
                toWorksDetail(data.productionId, data.id)
            }
            const onClickDelete = () => {
                deleteNotification(data.id, total)
            }
            return (
                <div className='notification-item' style={{ background: show ? '#e7f7ff' : '#fff' }} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
                    <div>
                        <div className='notification-item-time'>{data.state === 1 && <Userspot />}{`${moment(data.createdAt).format('YYYY.MM.DD\xa0HH:mm:ss')}`}</div>
                        {data.type === 4 ? <div className='notification-item-content'>
                            <div style={{ color: '#999999', fontSize: '16px' }}>@</div>
                            <img
                                style={{ width: '33px', height: '33px', margin: '0 10px', borderRadius: '33px' }}
                                src={data.toAvatarUrl}
                                alt='' />
                            <div>{`${data.toName}，${data.content}`}</div>
                            <div style={{ color: '#54C3D4', cursor: 'pointer' }} onClick={onClickProps}>@{data.productionName}</div>
                        </div>
                            :
                            <div className='notification-item-content'>
                                <img
                                    style={{ width: '33px', height: '33px', margin: '0 10px', borderRadius: '33px' }}
                                    src={data.fromAvatarUrl}
                                    alt='' />
                                <div>{`${data.fromName}${data.content}`}</div>
                                <div style={{ color: '#54C3D4', cursor: 'pointer' }} onClick={onClickProps}>@{data.productionName}</div>
                            </div>
                        }
                    </div>
                    {show && <img
                        onClick={onClickDelete}
                        style={{ width: '22px', height: '22px', cursor: 'pointer' }}
                        src={`${process.env.PUBLIC_URL}/assets/icon-delete.png`}
                        alt='' />}
                </div>
            )
        }
    )
)
export default Notificationitem;