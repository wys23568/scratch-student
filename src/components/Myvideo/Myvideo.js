import React, { useState, useEffect } from 'react';
import './Myvideo.less';
import { Row, Icon, Slider, Button } from 'antd';
import Hls from 'hls.js'
let isMove = false,
    timer = null;
const Myvideo = React.memo(
    (props) => {
        const { videoUrl, handleCancel, onClickToStudy } = props
        const [media, setMedia] = useState(null)
        const [playbackProgress, setPlaybackProgress] = useState(0)
        const [play, setPlay] = useState(false)
        const [full, setFull] = useState(false)
        const [volume, setVolume] = useState(0)
        const [duration, setDuration] = useState(0)
        const [showVoice, setShowVoice] = useState(false)
        const [mute, setMute] = useState(false)
        const [showControls, setShowControls] = useState(false);
        const [ready, setReady] = useState(true);
        useEffect(() => {
            document.getElementById("media").volume = 0.3;
            document.oncontextmenu = () => { return false }
            setVolume(30);
            var video = document.getElementById('media');
            if (Hls.isSupported()) {
                var hls = new Hls();
                hls.loadSource(videoUrl);
                hls.attachMedia(video);
            }
            return () => {
                document.onkeydown = null;
                clearTimeout(timer);
                document.oncontextmenu = () => { return true }
            }
        }, [videoUrl])
        const secondToDate = (val) => {
            var h = Math.floor(val / 3600) < 10 ? `0${Math.floor(val / 3600)}` : Math.floor(val / 3600),
                m = Math.floor((val / 60 % 60)) < 10 ? `0${Math.floor((val / 60 % 60))}` : Math.floor((val / 60 % 60)),
                s = Math.floor((val % 60)) < 10 ? `0${Math.floor((val % 60))}` : Math.floor((val % 60));
            return `${h}:${m}:${s}`
        }

        const onCanPlay = (e) => {
            setReady(false)
            setDuration(e.target.duration)
        }
        const onTimeUpdate = (e) => {
            if (media.readyState > 2) {
                ready && setReady(false)
            }
            else {
                !ready && setReady(true)
            }
            setPlaybackProgress(e.target.currentTime)
        }
        const FullScreen = (ele) => {
            if (ele.requestFullscreen) {
                ele.requestFullscreen();
            } else if (ele.mozRequestFullScreen) {
                ele.mozRequestFullScreen();
            } else if (ele.webkitRequestFullScreen) {
                ele.webkitRequestFullScreen();
            }
        }
        const exitFullscreen = (de) => {
            if (de.exitFullscreen) {
                de.exitFullscreen();
            } else if (de.mozCancelFullScreen) {
                de.mozCancelFullScreen();
            } else if (de.webkitCancelFullScreen) {
                de.webkitCancelFullScreen();
            }
        }

        const playVideo = () => {
            if (play) {
                setPlay(false)
                media.pause()
                return
            }
            else {
                setPlay(true)
                media.play()
            }

        }
        const changeScreen = () => {
            const userVideo = document.getElementById("user-video");
            document.onfullscreenchange = (e) => {
                if (document.fullscreenElement === null) {
                    setFull(false)
                }
            }
            if (full) {
                exitFullscreen(document)
                setFull(false)
                return
            }
            else {
                FullScreen(userVideo)
                setFull(true)
            }

        }
        const muteVolume = () => {
            if (mute) {
                setMute(false)
                return
            }
            setMute(true)
        }
        const changeVolume = (value) => {
            setVolume(value);
            media.volume = value / 100;
        }
        const changeCurrentTime = (value) => {
            setPlaybackProgress(value)
            media.currentTime = value
        }
        const onEnded = () => {
            setPlay(false)
        }
        const onMouseMoveShow = () => {
            isMove = true;
            clearTimeout(timer);
            !showControls && isMove && setShowControls(true)
            timer = setTimeout(function () {
                isMove = false;
                showControls && !isMove && setShowControls(false)
            }, 4000);
        }
        document.onkeydown = (ev) => {
            if (ev.keyCode === 32) {
                playVideo()
            }
        }
        return (
            <div id='user-video'
                onMouseMove={onMouseMoveShow}
                onMouseEnter={() => { setShowControls(true) }}
                onMouseLeave={() => { setShowControls(false) }} >
                {
                    !full && showControls && <div className='show-close-div'>
                        <Icon onClick={() => handleCancel()} className='show-colse-icon' type="close" />
                        <Button
                            onClick={() => onClickToStudy()}
                            className='show-close-button'
                            type='primary'>去学习</Button>
                    </div>
                }
                <video id='media'
                    onSeeking={() => { setReady(true) }}
                    onSeeked={() => { setReady(false) }}
                    preload='auto'
                    onCanPlay={onCanPlay}
                    onEnded={onEnded}
                    onClick={playVideo}
                    muted={mute}
                    onTimeUpdate={onTimeUpdate}
                    ref={el => setMedia(el)}
                >
                    您的浏览器不支持 video 标签。
            </video>
                {
                    showControls && <Row className='row-position' type='flex' justify='start'>
                        <div className='video-row' type='flex' justify='start' align='middle'>
                            <div className='video-row-play-button' onClick={playVideo}>{play ? <Icon type="pause-circle" /> : <Icon type="play-circle" />}</div>
                            <div className='video-row-play-time'>{secondToDate(playbackProgress)}</div>
                            <Slider style={{ flex: 1 }} onChange={changeCurrentTime} value={playbackProgress} max={parseInt(duration)} step={1}
                                tipFormatter={value => secondToDate(value)}
                            />
                            <div className='video-row-play-time-right'>{secondToDate(duration)}</div>
                        </div>
                        <div
                            onMouseEnter={() => { setShowVoice(true) }}
                            onMouseLeave={() => { setShowVoice(false) }}> {showVoice && <div className='volume'>
                                <Slider vertical onChange={changeVolume} value={volume} disabled={mute}
                                /></div>}
                            <div className='voice-img'><img
                                onClick={muteVolume}
                                style={{ width: '23px', height: '23px' }}
                                src={`${process.env.PUBLIC_URL}/assets/${mute ? 'icon-close-sound' : 'icon-open-sound'}.png`} alt='' /></div>

                        </div>

                        <div className='voice-img1' > <img onClick={changeScreen}
                            style={{ width: '23px', height: '23px', marginRight: '32px' }}
                            src={`${process.env.PUBLIC_URL}/assets/${full ? 'icon-exit-full-screen' : 'icon-full-screen'}.png`} alt='' />
                        </div>
                    </Row>
                }
                {
                    ready && <div className="user-defined">
                        <div className="rect1"></div>
                        <div className="rect2"></div>
                        <div className="rect3"></div>
                        <div className="rect4"></div>
                        <div className="rect5"></div>
                    </div>
                }
            </div>
        )
    }
)
export default Myvideo;