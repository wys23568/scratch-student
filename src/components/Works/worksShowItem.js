import React from 'react';
import './worksShowItem.less';
import { Row, Col, Icon } from 'antd';
import {
    withRouter
} from "react-router-dom";
import routerApi from 'Config/routerApi.js';
const Worksshowitem = React.memo(
    withRouter((props) => {
        const { detail = {} } = props;
        const toWorksDetail = () => {
            window.open(`${routerApi}#/works-show/${detail.id}`)
            // props.history.push(`/works-show/${detail.id}`)
        }
        return (
            <div className='works-show-item' >
                <img onClick={toWorksDetail} className='works-show-item-row1' src={detail.coverUrl} alt='' />
                <Row className='works-show-item-row2' >{detail.name} </Row>
                <Row className='works-show-item-row3'>
                    <Col className='row3-col'><Icon style={{ marginRight: '6px', fontSize: '17px' }} type="eye" theme="filled" />{detail.lookCount}</Col>
                    <Col ><Icon style={{ marginRight: '6px', fontSize: '17px' }} type="like" theme="filled" />{detail.likeCount}</Col>
                </Row>
                <Row type='flex' justify='start' align='middle' className='works-show-item-row4'>
                    <Col><img style={{ width: '26px', height: '26px', marginRight: '8px',borderRadius:'26px' }} src={detail.authorAvatar} alt='' /></Col>
                    <Col>{detail.authorName}</Col></Row>
            </div>
        )
    })
)
export default Worksshowitem;