import React, { PureComponent } from 'react';
import './commentItem.less';
import { Row, Col, Icon, Popover } from 'antd';
import moment from 'moment';
import styled from 'styled-components';
import Commentbox from './commentBox.js';
const Userlittle = styled.div`
cursor: pointer;
transition: all 0.3s;
&:hover{
    color:#54C3D4;
}
`
class Commentitem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            like: false,
            collected: false,
            reply: false,
            visible: false
        }
    }
    doLike = (likeFlag, id) => {
        const { likeCommentOrReply, commentId = '' } = this.props;
        likeCommentOrReply(likeFlag, id, commentId)
    }
    reply = () => {
        const { reply } = this.state
        this.setState({ reply: !reply })
    }
    reportComment = () => {
        const { reportType, worksOrCommentOrReplyReport, data = {} } = this.props;
        this.setState({
            visible: false,
        })
        worksOrCommentOrReplyReport(reportType, data.id)
    }
    deleteComment = () => {
        const { data = {}, deleteCommentOrReply, total } = this.props;
        this.setState({
            visible: false,
        })
        deleteCommentOrReply(data.id,total)
    }
    handleVisibleChange = visible => {
        this.setState({ visible });
    };
    render() {
        const { reply, visible } = this.state;
        const { data = {}, commentId, replySubmit, targetId, type } = this.props;
        const { fromAvatarUrl, fromName, createdAt, content, toName, likeCount, likeFlag, id, selfFlag } = data;
        return (
            <div className='comment-item'>
                <img className='comment-item-left' src={fromAvatarUrl} alt='' />
                <div className='comment-item-right'>
                    <Row className='right-row' type='flex' justify='space-between' align='middle'>
                        <Col style={{ display: 'flex' }} span={12}>
                            <div style={{ fontSize: '14px', fontWeight: 'bold', color: '#444444', marginRight: '14px' }}>{fromName}</div>
                            <div>{`${moment(createdAt).format('YYYY.MM.DD\xa0HH:mm')}`}</div>
                        </Col>
                        <Col className='right-col' span={12}>
                            <div onClick={this.reply} className='right-col-div' >
                                <Icon
                                    className='right-col-icon'
                                    style={{ color: reply ? '#FFBF00' : '#CCD0D5', marginRight: '7px' }}
                                    type="message" theme="filled" />
                                回复
                                </div>
                            <div onClick={this.doLike.bind(this, likeFlag, id)} className='right-col-div'>
                                <Icon
                                    className='right-col-icon'
                                    style={{ color: likeFlag === 2 ? '#FFBF00' : '#CCD0D5', marginRight: '7px' }}
                                    type="like" theme="filled" />
                                {
                                    likeCount
                                }
                            </div>
                            <div className='right-col-div'>

                                <Popover placement="bottom" content={<div>
                                    {selfFlag === 2 ? <Userlittle onClick={this.deleteComment} style={{ cursor: 'pointer' }}>删除</Userlittle> :
                                        <Userlittle onClick={this.reportComment}>举报</Userlittle>}
                                </div>}
                                    trigger="click"
                                    visible={visible}
                                    onVisibleChange={this.handleVisibleChange}
                                >
                                    <img className='right-col-div-img' src={`${process.env.PUBLIC_URL}/assets/icon-more-gray.png`} alt='' /> 更多
                                </Popover></div>
                        </Col>
                    </Row>
                    <Row type='flex' justify='start' align='middle'>
                        {toName && <div style={{ fontSize: '14px', fontWeight: 'bold', color: '#444444', marginRight: '14px' }}>{`@${toName}`}</div>}
                        {content}</Row>
                    <Row style={{ marginTop: '24px' }}>{reply && <Commentbox
                        id={commentId} onClickSubmit={replySubmit} targetId={targetId} type={type} closeCommentBox={this.reply.bind(this)} />}</Row>
                </div>
            </div>
        )
    }
}
export default Commentitem;