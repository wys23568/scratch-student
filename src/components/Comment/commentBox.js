import React, { useState } from 'react';
import { Input, Row, Col, Popover, Button } from 'antd';
import * as message from 'Message';
import styled from 'styled-components';
import { Picker } from 'emoji-mart';
import './commentBox.less';
import 'emoji-mart/css/emoji-mart.css';
const Userfooter = styled.div`
width:50px;
height: 17px;   
line-height:17px;
font-size:12px;
color:#999999;
position:absolute;
right: 15px;
bottom: 5px;
text-align:right;
`
const { TextArea } = Input;
const Commentbox = React.memo(
    ({ id, onClickSubmit, targetId = '', type = 0, closeCommentBox = () => { } }) => {
        const [value, setValue] = useState('');
        const [visible, setVisible] = useState(false)
        const onChange = (e) => {
            setValue(e.target.value)
        }
        const onSelect = (emoji) => {
            setValue(`${value}${emoji.native}`)
            setVisible(false)
        }
        const onClickButton = () => {
            if (!value) {
                message.info('内容不能为空呢~')
                return
            }
            closeCommentBox()
            setValue('')
            onClickSubmit(id, value, targetId, type)
        }
        const handleVisibleChange = visible => {
            setVisible(visible)
        };
        return (
            <Row>
                <Row className='row-position'>
                    <TextArea required
                        onChange={onChange}
                        maxLength={140}
                        value={value}
                        rows={6}
                        placeholder={'还有哪些地方需要改进呢？一起进步吧！'}></TextArea>
                    <Userfooter>{value.length}/140</Userfooter>
                </Row>
                <Row className='row-position-row' type='flex' justify='space-between' align='middle'>
                    <Col><Popover placement="rightBottom"
                        visible={visible}
                        onVisibleChange={handleVisibleChange}
                        content={<Picker
                            include={['people']}
                            title={''}
                            showSkinTones={false}
                            showPreview={false}
                            i18n={{
                                search: '搜索',
                                clear: '清除', // Accessible label on "clear" button
                                notfound: '未找到表情',
                                skintext: '选择你的默认肤色',
                                categories: {
                                    search: '',
                                    recent: '经常使用',
                                    people: '',
                                    nature: '动物与自然',
                                    foods: '食物和饮料',
                                    activity: '活动',
                                    places: '旅行和地方',
                                    objects: '对象',
                                    symbols: '符号',
                                    flags: '旗帜',
                                    custom: '自定义',
                                },
                                categorieslabel: '表情符号的类别', // Accessible title for the list of categories
                                skintones: {
                                    1: '默认肤色',
                                    2: '轻肤色',
                                    3: '中轻质肤色',
                                    4: '中等肤色',
                                    5: '偏深色肤色',
                                    6: '深肤色',
                                },
                            }}
                            onSelect={onSelect} set='apple' />} trigger="click">
                        <img style={{ width: '22px', height: '22px', cursor: 'pointer' }} src={`${process.env.PUBLIC_URL}/assets/icon-smile.png`} alt='' />
                    </Popover></Col>
                    <Col className='row-position-row-col' span={12}>
                        <Button
                            onClick={onClickButton}
                            style={{ borderRadius: '31px', width: '89px' }}
                            type='primary'>发送</Button>
                    </Col>
                </Row>
            </Row>
        )
    }
)
export default Commentbox;